# Async-Await supplement

This repository contains the software artefacts accompanying the paper :

> A Combinatorial Study of Async/Await Processes
> by M. Dien, A. Genitrini and F. Peschanski

All the code is unless stated otherwise distributed under the GPL 3.0 license
(cf. gpl-3.0.txt).

# Basic dependencies

We list the various dependencies required to compile and run
the experiment described in the paper.

- Python 3.8 or later for scripting
- A recent C/C++ compiler (e.g. GNU GCC 9.4.0)

The experiment has been conducted on various Linux machines running
either a recent Ubuntu or Arch distribution. It is also known to
work in WSL2 on Windows 10, Intel architecture. 

## Flintegrator and sampler

Flintegrator and sampler are elementary blocks written in C(99) using
external libraries :

- [gmp](https://gmplib.org/): The GNU Multiple Precision Arithmetic Library 
- [mpfr](https://www.mpfr.org/):  The Multiple-Precision Binary Floating-Point Library With Correct Rounding
- [flint](http://flintlib.org/doc/index.html): Fast Library for Number Theory

At the moment, we use Flint version 2.8.0.

There are various ways to install these libraries but the manual compilation
and installation, following the instructions in the documentation of the libraries.

After having installed the dependencies, there  is a `Makefile` in the `integrator/` subdirectory.
Just use `make` in that subdirectory to compile the integrator and sampler 
programs.


# Experiment 1 : generation of process structures

```
$ cd intervalorders
$ python gen_chord_process.py --count 40
===================== COUNT =====================
The number of size 40 chord processes is: 31087931945514679251276758644207574163542

$ python gen_chord_process.py --sample 10
===================== SAMPLE =====================
The chord process 183847 as been saved in chord_process_10_183847.dot
$ cat chord_process_10_183847.dot
graph {
  layout = dot
  rankdir = TB
  node [ shape="none" ]

  /* elements */
  10 [label="10"];
  107 [label="107"];
  3 [label="3"];
  106 [label="106"];
  9 [label="9"];
  5 [label="5"];
  105 [label="105"];
  2 [label="2"];
  104 [label="104"];
  1 [label="1"];
  103 [label="103"];
  4 [label="4"];
  102 [label="102"];
  8 [label="8"];
  101 [label="101"];
  6 [label="6"];
  7 [label="7"];
  100 [label="100"];
  /* relations */
  10 -- 107;  3 -- 107;  106 -- 10;  9 -- 106;  5 -- 106;  105 -- 9;  2 -- 105;  104 -- 105;
  104 -- 3;  1 -- 104;  103 -- 104;
  103 -- 2;  4 -- 103;  102 -- 5;
  102 -- 103;  8 -- 102;  101 -- 8;  6 -- 101;  7 -- 101;  100 -- 1;
  100 -- 4;
  100 -- 6;
  100 -- 7;
}

$ python gen_chord_process.py --unrank 4242 --type matrix 10
===================== UNRANK =====================
The chord process 4242 as been saved in chord_process_10_4242.mat
$ cat chord_process_10_4242.mat
0 0 0 0 0 0 0 0 0 0 0 0 1 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
0 0 0 0 0 0 0 0 0 0 0 1 0 0 0
0 0 0 0 0 0 0 0 0 0 0 1 0 0 0
0 0 0 0 0 0 0 0 0 0 0 1 0 0 0
0 0 0 0 0 0 0 0 0 0 0 1 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 1 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
0 0 0 0 0 0 0 0 0 0 0 0 0 0 1
1 0 1 1 1 1 0 0 0 0 0 0 0 0 0
0 1 0 0 0 0 0 0 0 0 0 0 1 0 0
0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 1 1 1 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
```

# Experiment 2 : counting linear extensions

The script `benchmarks/bench_counting_chord_processes.py` can be used to
enumerate or sample chord processes then `bitcount/modbitcount.py`
on them. An help is provided by the `--help` command line argument.

Some examples :
```
$ cd benchmarks
$ python bench_counting_chord_processes.py --random 10 -n 5
Generating random chord processes of size = 10
#133492 [0, 1, 2, 1, 2, 3, 0, 5, 1, 0] : 46800 (time = 1.31966053799988 s)
#116829 [0, 1, 2, 0, 2, 4, 5, 0, 5, 1] : 7952 (time = 1.2329856270000619 s)
#98079 [0, 1, 1, 2, 3, 4, 5, 0, 3, 1] : 1568 (time = 1.2220448089999536 s)
#180821 [0, 1, 2, 3, 2, 4, 1, 0, 5, 1] : 5544 (time = 1.2231493389999741 s)
#29932 [0, 0, 1, 2, 3, 4, 1, 3, 0, 2] : 3952 (time = 1.2087471919999189 s)
========
Total flint time   = 6.2065875049997885 s
```

```
$ python bench_counting_chord_processes.py 20 --mini 1000 --maxi 100000 --step 10000 -n 10
Enumerating chord processes of size = 20
#10000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 3, 4, 0, 1): 21693982464000 (time = 1.2057406509998145 s)
#20000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 1, 0, 1, 2, 5, 6): 360209203200 (time = 1.2049656550000236 s)
#30000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 1, 4, 2, 4): 366915225600 (time = 1.2091040130001147 s)
#40000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 2, 3, 0, 3, 0): 533416181760000 (time = 1.2934535829999732 s)
#50000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 2, 4, 1, 0, 3, 4): 7964519270400 (time = 1.2188443590000588 s)
#60000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 2, 4, 1, 2, 2): 6136010496000 (time = 1.3191258510000807 s)
#70000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 3, 3, 0, 3, 2, 0): 14055822950400 (time = 1.2199866249998195 s)
#80000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 2, 2, 2, 3, 0): 528817766400 (time = 1.210865265999928 s)
#90000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 2, 2, 0, 0, 0, 1, 1): 26048107008000 (time = 1.2248226889998932 s)
#100000  (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 1, 4, 2, 3, 3): 6231012480000 (time = 1.2322657079998862 s)
========
Total flint time   = 12.339174399999592 s
```


# Experiment 3 : uniform random generation of execution paths

The script `benchmarks/bench_sampling_chord_processes.py` can be used to
enumerate or sample chord processes then run `bitcount/modbitsample.py`
on them. An help is provided by the `--help` command line argument.

Some examples :
```
$ python bench_sampling_chord_processes.py 10 --random -n 20
Generating random chord processes of size = 10
========
Chord process #143128 : 50624 lin. exts.
[2, 11, 6, 0, 7, 1, 8, 12, 13, 3, 9, 14, 5, 4, 10, 15]
[11, 6, 2, 7, 8, 1, 12, 3, 0, 13, 5, 9, 4, 14, 10, 15]
[12, 1, 11, 0, 2, 6, 8, 7, 3, 13, 9, 5, 14, 4, 10, 15]
[11, 6, 7, 0, 1, 3, 2, 8, 12, 13, 5, 9, 14, 4, 10, 15]
[2, 0, 11, 6, 1, 7, 8, 12, 13, 5, 3, 4, 9, 14, 10, 15]
[2, 11, 6, 7, 1, 8, 12, 13, 5, 3, 9, 4, 14, 0, 10, 15]
[11, 6, 2, 7, 0, 3, 1, 8, 12, 13, 5, 9, 4, 14, 10, 15]
[11, 6, 7, 2, 0, 1, 8, 3, 12, 13, 9, 5, 4, 14, 10, 15]
[11, 2, 6, 7, 1, 3, 8, 12, 13, 0, 9, 5, 14, 4, 10, 15]
[2, 1, 11, 6, 7, 0, 8, 12, 3, 13, 9, 5, 4, 14, 10, 15]
[2, 0, 11, 6, 7, 8, 1, 3, 12, 13, 5, 9, 14, 4, 10, 15]
[12, 2, 11, 8, 1, 6, 7, 13, 3, 9, 0, 14, 5, 4, 10, 15]
[11, 6, 2, 7, 8, 3, 12, 0, 1, 13, 5, 4, 9, 14, 10, 15]
[11, 6, 7, 2, 8, 1, 0, 12, 13, 3, 9, 5, 14, 4, 10, 15]
[2, 11, 0, 6, 1, 7, 8, 3, 12, 13, 5, 4, 9, 14, 10, 15]
[0, 11, 6, 7, 2, 3, 8, 12, 1, 13, 9, 5, 4, 14, 10, 15]
[1, 11, 2, 6, 7, 8, 12, 13, 3, 5, 9, 14, 0, 4, 10, 15]
[9, 12, 2, 11, 1, 0, 8, 6, 14, 3, 7, 13, 5, 4, 10, 15]
[11, 6, 7, 1, 2, 8, 12, 13, 5, 3, 4, 9, 14, 0, 10, 15]
[11, 6, 7, 2, 0, 1, 8, 12, 13, 5, 4, 3, 9, 14, 10, 15]
Total flint sampling time   = 0.076283 s
Total flint counting time   = 0.000523 s
Total flint time   = 1.383402167999975 s
```


```
$ python bench_sampling_chord_processes.py 10 --maxi 20000 --step 10000 -n 10
Enumerating chord processes of size = 10
#10000  (0, 0, 1, 0, 2, 0, 3, 4, 0, 1):
Number of linear extensions 5760
[11, 3, 0, 12, 2, 13, 4, 5, 14, 7, 6, 8, 1, 9, 15, 16, 10]
[11, 3, 12, 2, 13, 4, 0, 5, 14, 7, 8, 6, 1, 9, 15, 16, 10]
[11, 3, 12, 2, 13, 5, 0, 14, 7, 6, 4, 8, 9, 15, 1, 16, 10]
[0, 11, 3, 12, 2, 13, 5, 14, 7, 4, 8, 9, 1, 6, 15, 16, 10]
[11, 3, 12, 2, 13, 5, 6, 14, 0, 7, 8, 4, 9, 15, 1, 16, 10]
[11, 3, 12, 2, 13, 5, 6, 14, 4, 0, 7, 1, 8, 9, 15, 16, 10]
[11, 3, 12, 2, 13, 0, 5, 14, 7, 6, 1, 8, 4, 9, 15, 16, 10]
[11, 3, 12, 2, 13, 5, 0, 14, 7, 8, 4, 1, 9, 15, 6, 16, 10]
[11, 0, 3, 12, 2, 13, 4, 5, 14, 7, 8, 1, 9, 15, 6, 16, 10]
[11, 3, 0, 12, 2, 13, 5, 4, 14, 7, 1, 6, 8, 9, 15, 16, 10]
flint sampling time   = 0.054089 s
flint counting time   = 0.000622 s
#20000  (0, 0, 1, 2, 1, 0, 1, 2, 5, 6):
Number of linear extensions 464
[0, 1, 5, 13, 2, 6, 7, 14, 3, 15, 9, 4, 8, 16, 12, 17, 11, 10]
[0, 1, 5, 13, 6, 7, 14, 2, 3, 15, 4, 9, 8, 16, 12, 17, 11, 10]
[0, 1, 9, 5, 8, 16, 3, 2, 13, 6, 7, 14, 15, 4, 12, 17, 11, 10]
[0, 1, 5, 13, 2, 6, 7, 14, 15, 3, 9, 8, 16, 4, 12, 17, 11, 10]
[0, 1, 2, 5, 13, 6, 7, 14, 3, 15, 9, 8, 16, 4, 12, 17, 11, 10]
[0, 1, 5, 13, 2, 6, 7, 14, 15, 3, 9, 8, 4, 16, 12, 17, 11, 10]
[0, 1, 2, 5, 13, 6, 7, 14, 15, 3, 9, 4, 8, 16, 12, 17, 11, 10]
[0, 1, 5, 13, 6, 2, 7, 14, 15, 3, 4, 9, 8, 16, 12, 17, 11, 10]
[0, 1, 5, 13, 6, 7, 14, 2, 15, 3, 4, 9, 8, 16, 12, 17, 11, 10]
[0, 1, 5, 13, 6, 7, 14, 2, 15, 4, 3, 9, 8, 16, 12, 17, 11, 10]
flint sampling time   = 0.026036 s
flint counting time   = 0.000342 s
========
Total flint sampling time   = 0.080125 s
Total flint counting time   = 0.000964 s
Total flint time   = 2.5209461089998513 s
```
----
Copyright © 2022 The Parco Project and contributors under the GPL-3.0 (cf. gpl-3.0.txt)
