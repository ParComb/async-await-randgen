'''
In this benchmark, we study the count of linear extensions
for Posets corresponding to chord processes of a given size,
using our own "bitcount" algorithm.
'''

import sys
sys.path.append("../lib")
sys.path.append("../bitrec")
sys.path.append("../mod-decomp/src")
sys.path.append("../intervalorders")

import os
import subprocess
from timeit import default_timer as timer



import poset
from ascentseq import gen_ascentseq, unrank_aseq, ascent_sequence_to_poset, poset_to_ascent_sequence
from randgen import random_ascent_sequence
from fjseq import poset_from_fjseq, enum_fjseq, proc_from_fjseq, fjseq_from_poset
from ascent_fj_corresp import fjseq_from_ascentseq
from classify_intord import classify_ascentseq
from poclassify import classify_po
import twostructure as ts
import moddecomp as md

import posetio

import bitrec

def gen_chord_posets(size):
    for aseq in gen_ascentseq(size):
        proc = proc_from_fjseq(fjseq_from_ascentseq(aseq))
        po = poset.Poset(proc.to_dict())
        po.transitive_reduction()
        yield (aseq, po)

def gen_random_chord_posets(size):
    rank, aseq = random_ascent_sequence(size)
    proc = proc_from_fjseq(fjseq_from_ascentseq(aseq))
    po = poset.Poset(proc.to_dict())
    po.transitive_reduction()
    return (rank, aseq, po)

FLINT_COMMAND = "../bitcount/modbitcount.py"

def run_flint(filename):
    start = timer()
    proc = subprocess.run(["python3", FLINT_COMMAND, '--flint', '--quiet', filename], capture_output=True)
    countstr = proc.stdout[:-1].decode('utf-8')
    count = int(countstr)
    end = timer()
    return count, (end - start) 

def enumeration_benchmark(config):
    if not config.silent:
        print(f"Enumerating chord processes of size = {config.size}")

    flint_time = 0

    nb_generated = 0
    rank = -1

    rankstep = -1
    for aseq, chord_po in gen_chord_posets(config.size):
        rank += 1
        rankstep += 1
        if 0 < rankstep < config.step:
            # skip this one
            continue
        else:
            rankstep = 0
        if rank < config.mini:
            # skip this one
            continue
        if config.maxi is not None and rank > config.maxi:
            break
        if config.number and nb_generated >= config.number:
            break

        if not config.silent:
            print(f"#{rank}  {aseq}: ", end='')

        nb_generated += 1
        filename = f"chord_{config.size}_{rank}.txt"
        with open(filename, 'w') as chord_file:
            posetio.save_poset(chord_po.to_dict(), filename, mode='matrix')
        flint_count, elapsed = run_flint(filename)
        flint_time += elapsed
        print("{} (time = {} s)".format(flint_count, elapsed))

        if not config.keep_files:
            os.remove(filename)

    print("========")
    print("Total flint time   = {} s".format(flint_time))

            
def random_benchmark(config):
    if not config.silent:
        print(f"Generating random chord processes of size = {config.size}")

    flint_time = 0
    nb_generated = 0

    if config.number is None:
        config.number = 1
    
    for k in range(config.number):
        rank, aseq, chord_po = gen_random_chord_posets(config.size)

        if not config.silent:
            print(f"#{rank} {aseq} : ", end='')

        nb_generated += 1
        filename = f"chord_{config.size}_{nb_generated}.txt"
        with open(filename, 'w') as chord_file:
            posetio.save_poset(chord_po.to_dict(), filename, mode='matrix')
        flint_count, elapsed = run_flint(filename)
        flint_time += elapsed
        print("{} (time = {} s)".format(flint_count, elapsed))

        if not config.keep_files:
            os.remove(filename)

    print("========")
    print("Total flint time   = {} s".format(flint_time))

            
if __name__ == """__main__""":
    import argparse
    parser = argparse.ArgumentParser(description="Benchmark script for Chord Process")
    parser.add_argument("size", metavar='<size>', type=int, help="the size of processes to generate")
    parser.add_argument("--mini", type=int, action='store', default=1, help="the minimal rank for enumeration")
    parser.add_argument("--maxi", type=int, action='store', default=None, help="the maximal rank for enumeration")
    parser.add_argument("--step", type=int, action='store', default=1, help="the rank step for enumeration")
    parser.add_argument("--random", "-r", action='store_true', help="use randomly generated processes")
    parser.add_argument("--number", "-n", action='store', type=int, default=None, help="The (maximal) number of processes to generate/enumerate")
    parser.add_argument("--keep-files", action='store_true', help="Keep generated process files")
    parser.add_argument("--silent", action='store_true', help="Silent mode (only show timings)")
    parser.add_argument("--seed", action='store', type=int, default=None, help="the seed for the random generator")

    config = parser.parse_args()
    #print(config)

    if not config.random:
        enumeration_benchmark(config)    
    else:
        if config.seed:
            import random
            random.seed(config.seed)
        random_benchmark(config)


    
    
