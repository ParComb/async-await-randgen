'''
In this benchmark, we study the sampling of linear extensions
for Posets corresponding to chord processes of a given size, using
our own "bitcount" algorithm

'''

import sys
sys.path.append("../lib")
sys.path.append("../bitrec")
sys.path.append("../mod-decomp/src")
sys.path.append("../intervalorders")

import os
import subprocess
from timeit import default_timer as timer

import poset
from ascentseq import gen_ascentseq, unrank_aseq, ascent_sequence_to_poset, poset_to_ascent_sequence
from randgen import random_ascent_sequence
from fjseq import poset_from_fjseq, enum_fjseq, proc_from_fjseq, fjseq_from_poset
from ascent_fj_corresp import fjseq_from_ascentseq
from classify_intord import classify_ascentseq
from poclassify import classify_po
import twostructure as ts
import moddecomp as md

import posetio

import bitrec

def gen_chord_posets(size):
    for aseq in gen_ascentseq(size):
        proc = proc_from_fjseq(fjseq_from_ascentseq(aseq))
        po = poset.Poset(proc.to_dict())
        po.transitive_reduction()
        yield (aseq, po)

def gen_random_chord_posets(size):
    rank, aseq = random_ascent_sequence(size)
    proc = proc_from_fjseq(fjseq_from_ascentseq(aseq))
    po = poset.Poset(proc.to_dict())
    po.transitive_reduction()
    return (rank, aseq, po)

FLINT_COMMAND = "../bitcount/modbitsample.py"

def run_flint(filename, number=1):
    start = timer()
    proc = subprocess.run(["python3", FLINT_COMMAND, filename,
                           '--quiet', '--bench',
                           '--number', str(config.number)],
                          capture_output=True)
    lines = proc.stdout.decode('utf-8').split("\n")

    lin_exts = lines[-number-4:-4]
    
    le_count = int(lines[-4])
    counting_time = float(lines[-3])
    sampling_time = float(lines[-2])
    end = timer()
    return lin_exts, le_count, sampling_time, counting_time, (end - start)


def enumeration_benchmark(config):
    if not config.silent:
        print(f"Enumerating chord processes of size = {config.size}")

    flint_sampling_time = 0
    flint_counting_time = 0
    flint_total_time = 0


    nb_generated = 0
    rank = -1

    rankstep = -1
    for aseq, chord_po in gen_chord_posets(config.size):
        rank += 1
        rankstep += 1
        if 0 < rankstep < config.step:
            # skip this one
            continue
        else:
            rankstep = 0
        if rank < config.mini:
            # skip this one
            continue
        if config.maxi is not None and rank > config.maxi:
            break
        if config.number and nb_generated >= config.number:
            break

        nb_generated += 1

        if not config.silent:
            print(f"#{rank}  {aseq}:")
        
        filename = f"chord_{config.size}_{rank}.txt"
        with open(filename, 'w') as chord_file:
            posetio.save_poset(chord_po.to_dict(), filename, mode='matrix')
                
        lin_exts, le_count, sampl_time, count_time, elapsed = run_flint(filename, config.number)
        flint_total_time += elapsed
        flint_sampling_time += sampl_time
        flint_counting_time += count_time

        print(f"Number of linear extensions {le_count}")
        print("\n".join(lin_exts))
        print("flint sampling time   = {} s".format(sampl_time))
        print("flint counting time   = {} s".format(count_time))

        
        if not config.keep_files:
            os.remove(filename)

    print("========")
    print("Total flint sampling time   = {} s".format(flint_sampling_time))
    print("Total flint counting time   = {} s".format(flint_counting_time))
    print("Total flint time   = {} s".format(flint_total_time))

            
def random_benchmark(config):
    if not config.silent:
        print(f"Generating random chord processes of size = {config.size}")

    flint_sampling_time = 0
    flint_counting_time = 0
    flint_total_time = 0

    nb_generated = 0

    if config.number is None:
        config.number = 1

    rank, aseq, chord_po = gen_random_chord_posets(config.size)

    filename = f"chord_{config.size}_{rank}.txt"
    with open(filename, 'w') as chord_file:
        posetio.save_poset(chord_po.to_dict(), filename, mode='matrix')
    
    lin_exts, le_count, sampl_time, count_time, elapsed = run_flint(filename, config.number)
    flint_total_time += elapsed
    flint_sampling_time += sampl_time
    flint_counting_time += count_time
    
    if not config.keep_files:
        os.remove(filename)

    print("========")
    print(f"Chord process #{rank} : {le_count} lin. exts.")
    print("\n".join(lin_exts))
    print("Total flint sampling time   = {} s".format(flint_sampling_time))
    print("Total flint counting time   = {} s".format(flint_counting_time))
    print("Total flint time   = {} s".format(flint_total_time))
            
if __name__ == """__main__""":
    import argparse
    parser = argparse.ArgumentParser(description="Benchmark script for Chord Process")
    parser.add_argument("size", metavar='<size>', type=int, help="the size of processes to generate")
    parser.add_argument("--mini", type=int, action='store', default=1, help="the minimal rank for enumeration")
    parser.add_argument("--maxi", type=int, action='store', default=None, help="the maximal rank for enumeration")
    parser.add_argument("--step", type=int, action='store', default=1, help="the rank step for enumeration")
    parser.add_argument("--random", "-r", action='store_true', help="use randomly generated processes")
    parser.add_argument("--number", "-n", action='store', type=int, default=None, help="The number of processes to enumerate / the number of linear extension to sample (when --random) ")
    parser.add_argument("--keep-files", action='store_true', help="Keep generated process files")
    parser.add_argument("--silent", action='store_true', help="Silent mode (only show timings)")
    parser.add_argument("--seed", action='store', type=int, default=None, help="the seed for the random generator")

    config = parser.parse_args()
    #print(config)

    if not config.random:
        enumeration_benchmark(config)    
    else:
        if config.seed:
            import random
            random.seed(config.seed)
        random_benchmark(config)


    
    
