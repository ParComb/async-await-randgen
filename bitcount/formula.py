class Polynomial:
    def is_scalar(self, val=None):
        return False

class Sum(Polynomial):
    def __init__(self, polys):
        self.polys = tuple(polys)
        
    def size(self):
        s = 1
        for pol in self.polys:
            s += pol.size()
        return s
        
    def copy(self):
        return Sum(self.polys)
    
    def __hash__(self):
        return hash(self.polys)

    def __eq__(self, other):
        if not isinstance(other, Sum):
            return False
        return self.polys == other.polys

    def __lt__(self, other):
        if isinstance(other, Monomial):
            return False # after monomials (arbitrary)
        if isinstance(other, Prod):
            return True # before products
        return self.polys < other.polys

    def __repr__(self):
        return "Sum({})".format(", ".join( (repr(p) for p in self.polys)))

    def __str__(self):
        return "(" + " + ".join(str(p) for p in self.polys) + ")"

    def latex(self):
        return " + ".join(p.latex() for p in self.polys)

class Prod(Polynomial):
    def __init__(self, polys):
        self.polys = tuple(polys)

    def size(self):
        s = 1
        for pol in self.polys:
            s += pol.size()
        return s

    def copy(self):
        return Prod(self.polys)

    def __hash__(self):
        return hash(self.polys)

    def __eq__(self, other):
        if not isinstance(other, Prod):
            return False
        return self.polys == other.polys
        
    def __lt__(self, other):
        if not isinstance(other, Prod):
            return False # products last (arbitrary)
        return self.polys < other.polys

    def __sub__(self, pol):
        return sum(self, -pol)

    def __neg__(self):
        return Prod(self.polys + [scalar(-1)])

    def __repr__(self):
        return "Prod({})".format(", ".join( (repr(p) for p in self.polys)))

    def __str__(self):
        return "(" + " * ".join(str(p) for p in self.polys) + ")"

    def latex(self):
        return " \times ".join(p.latex() for p in self.polys)

class Coef:
    def __init__(self, num, den = 1):
        if isinstance(num, int):
            self.num = num
            self.den = den
        elif isinstance(num, Coef):
            self.num = num.num
            self.den = num.den
        else:
            raise ValueError("Cannot create Coef from: {}".format(num))

    def __hash__(self):
        return hash(self.num) + hash(self.den)
    
    def __eq__(self, other):
        if isinstance(other, int):
            if other == 1:
                return self.num == self.den
            elif other == 0:
                return self.num == 0
            else:
                return self.den == 1 and self.num == other
        elif isinstance(other, Coef):
            return self.num == other.num and self.den == other.den
        else:
            return False

    def __lt__(self, other):
        assert isinstance(other, Coef)
        # we don't need a real ordering
        return self.num < other.num or self.den < other.den

    def __add__(self, other):
        assert isinstance(other, Coef)
        # XXX: this will generate big numbers, but at least there's no pgcd
        nnum = self.num * other.den + other.num * self.den
        nden = self.den * other.den
        return Coef(nnum, nden)

    def __mul__(self, other):
        assert isinstance(other, Coef)
        return Coef(self.num * other.num, self.den * other.den)

    def __truediv__(self, other):
        if isinstance(other, Coef):
            return Coef(self.num * other.den, self.den * other.num)
        elif isinstance(other, int):
            return Coef(self.num, self.den * other)
        else:
            raise NotImplementedError("Div not implemented for: {}".format(other))

    def __pow__(self, other):
        assert isinstance(other, int)
        return Coef(self.num ** other, self.den ** other)

    def __neg__(self):
        return Coef(-self.num, self.den)

    def eval(self):
        return Fraction(self.num, self.den)

    def __str__(self):
        if self.num == 0 or self.den == 1:
            return f"{self.num}"
        else:
            return f"{self.num}/{self.den}"

    def __repr__(self):
        return f"Coef({self.num}, {self.den})"

class MVars:
    def __init__(self, svardegs):
        # vset = set()
        # for (v,d) in svardegs:
        #     assert v not in vset
        #     vset.add(v)
        #     assert isinstance(v, str)
        #     assert isinstance(d, int)
        self.vs = {v : d for (v,d) in svardegs}
        self.vstr = ""
        self.maxdeg = 0
        for (v,d) in svardegs:
            if d > 1:
                self.vstr += f"{v}^{d}"
            else:
                self.vstr = f"{v}"
            if d > self.maxdeg:
                self.maxdeg = d
        # How the following distributes ?
        self.hashval = hash(svardegs)

    def __eq__(self, other):
        return self.vs == other.vs

    def __lt__(self, other):
        # the maximal degree is taken first
        # to discriminate quickly
        if self.maxdeg < other.maxdeg:
            return True
        elif self.maxdeg > other.maxdeg:
            return False

        return self.vstr < other.vstr

    def is_empty(self):
        return len(self.vs) == 0

    def __or__(self, other):
        if other.is_empty():
            return self
        vds = []
        for x in set(self.vs.keys()) | set(other.vs.keys()):
            d1 = self.vs.get(x, 0)
            d2 = other.vs.get(x, 0)
            vds.append( (x, d1 + d2) )
        return mvars(vds)

    def __contains__(self, v):
        return v in self.vs

    def __len__(self):
        return len(self.vs)

    def __getitem__(self, v):
        return self.vs[v]

    def remove_var(self, v):
        if v in self.vs:
            return mvars(( (x,d) for (x,d) in self.vs.items() if x != v))
        else:
            return self

    def __hash__(self):
        return self.hashval

    def __str__(self):
        return self.vstr

    def __repr__(self):
        return "MVars({})".format(repr(self.vs))

class Monomial(Polynomial):
    def __init__(self, coef, vs):
        self.coef = Coef(coef)
        if isinstance(vs, MVars):
            self.vs = vs
        elif isinstance(vs, (list,tuple)):
            self.vs = MVars(vs)
        else:
            raise ValueError("Not a MVars object or a tuple or a list: {}".format(repr(vs)))

    def is_scalar(self, val=None):
        if len(self.vs) > 0:
            return False
        if val is None:
            return True
        else:
            return self.coef == val

    def size(self):
        return len(self.vs)

    def copy(self):
        return Monomial(self.coef, self.vs)
        
    def __hash__(self):
        return hash(self.coef) + hash(self.vs)

    def __eq__(self, other):
        if not isinstance(other, Monomial):
            return False
        return self.coef == other.coef and self.vs == other.vs

    def __lt__(self, other):
        if not isinstance(other, Monomial):
            return True  # monomials always on the left (?)

        if self.vs == other.vs:
            return self.coef < other.coef

        return self.vs < other.vs
        
    def __neg__(self):
        return Monomial(-self.coef, self.vs)

    def __add__(self, other):
        if isinstance(other, Monomial):
            assert(self.vs == other.vs)
            return Monomial(self.coef + other.coef, self.vs)
        raise NotImplementedError("Addition is only for monomials")
        
    def remove_var(self, v):
        if len(self.vs) == 1 and v in self.vs:
            return scalar(self.coef)
        elif v not in self.vs:
            return self
        else:
            return Monomial(self.coef,
                            self.vs.remove_var(v))

    def __pow__(self, i):
        assert(isinstance(i, int))
        if self.is_scalar():
            return scalar(self.coef**i)
        else:
            return Monomial(self.coef**i, [(k, v*i) for k,v in self.vs.vs.items()])
        
    def __mul__(self, pol):
        if isinstance(pol, Monomial):
            ncoef =  pol.coef*self.coef
            nvs = self.vs | pol.vs
            return Monomial(ncoef, nvs)
        else:
            raise NotImplementedError()
        

    def __truediv__(self, s):
        if self.is_scalar() and isinstance(s, Monomial) and s.is_scalar():
            return scalar(self.coef/s.coef)
        else:
            raise NotImplementedError()

    def __repr__(self):
        return "Monomial({}, {})".format(self.coef, self.vs)

    def __str__(self):
        if self.vs.is_empty():
            return str(self.coef)
        
        ret = ""
        if self.coef != 1:
            ret += str(self.coef) + "*"
        #print("DEBUG={}".format(repr(self)))
        ret += str(self.vs)
        return ret

    def latex(self):
        ret = ""
        if self.coef != 1:
            ret += str(self.coef)
        ret += str(self.vs)
        return ret

def scalar(n):
    assert(isinstance(n, (int, Coef)))
    return Monomial(n, tuple())

class Int(Polynomial):
    def __init__(self, poly, v, vlo, vhi):
        self.poly = poly
        self.v = v
        self.vlo = vlo
        self.vhi = vhi

    def size(self):
        return 1 + self.poly.size()

    def copy(self):
        return Int(self.poly.copy(), self.v, self.vlo, self.vhi)
        
    def __repr__(self):
        return "Int({}, {}, {}, {})".format(repr(self.poly), repr(self.v), repr(self.vlo), repr(self.vhi)) 

    def __str__(self):
        return "Int_{}^{} {} d{}".format(self.vlo, self.vhi, str(self.poly), str(self.v))

    def latex(self):
        return "\\int_{{{}}}^{{{}}} {} d{}".format(self.vlo, self.vhi, str(self.poly), str(self.v))

def make_var(s):
    if not isinstance(s, str):
        s = "x_" + str(s)
    return Monomial(1, ((s,1),))
