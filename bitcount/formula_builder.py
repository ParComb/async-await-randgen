from formula import *
from math import factorial
from collections import defaultdict
from poset import Poset, get_BITE_vertices

def poset_of_ts(ts):
    if len(ts.colors) != 2:
        raise NotImplementedError("two-structures with more than 2 colors are not handled")
    p = Poset()    
    preds = {x : set() for xs in ts.modules for x in xs}

    for (u,v) in ts.colors[1]:
        us = list(ts.node_to_module(u))
        vs = list(ts.node_to_module(v))
        for i in range(len(us)-1):
            preds[us[i+1]].add(us[i])
        for i in range(len(vs)-1):
            preds[vs[i+1]].add(vs[i])
        preds[vs[0]].add(us[-1])
        
    p.predecessors = preds
    return p

def basic_strategy(poset):
    ints = []
    while not poset.isempty():
        poset.transitive_reduction()
        b,i,t,e = get_BITE_vertices(poset)
        x = (b+t+i+e)[0]
        predx = poset.predecessors[x]
        succx = poset.successors(x)
        lo = make_var(list(predx)[0]) if predx else scalar(0)
        hi = make_var(list(succx)[0]) if succx else scalar(1)
        ints.append((x, lo, hi))
        poset.remove(x)
    return ints

def build_formula_from_prime_graph(pg, strategy=basic_strategy):
    po = poset_of_ts(pg)
    return build_formula_from_poset(po, strategy)
    

def build_formula_from_poset(po, strategy=basic_strategy):
    # import pdb; pdb.set_trace()
    ints = strategy(po) # détruit le poset
    formula = scalar(1)
    while ints:
        x, lo, hi = ints.pop(0)
        # print(x,lo,hi,qg.node_to_module(x))
        formula = Int(formula, make_var(x), lo, hi)
    return formula
