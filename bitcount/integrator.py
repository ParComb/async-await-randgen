
import subprocess

from formula import scalar, Int, Prod, Monomial
from fractions import Fraction

SAGE_OK = False
try:
    from sage.all import integrate, var, factorial, QQ, prod, PolynomialRing
    SAGE_OK = True
except:
    pass

HSINTEGRATOR_CMD_DEFAULT = "integrator-exe"
HSINTEGRATOR_OK = False
def init_hsintegrator(cmd=None):
    global HSINTEGRATOR_OK
    if cmd is None:
        cmd = HSINTEGRATOR_CMD_DEFAULT

    try:
        subprocess.run(cmd, capture_output=True)
        HSINTEGRATOR_OK = True
    except:
        pass

def integrate_hs(cmd=None):
    raise NotImplementedError("HS integrator not supported")

MAXIMA_CMD_DEFAULT = ["maxima"]
MAXIMA_OK = False
def init_maxima(cmd=None):
    global MAXIMA_OK, MAXIMA_CMD_DEFAULT
    if cmd is None:
        cmd = MAXIMA_CMD_DEFAULT
        
    try:
        subprocess.run([*cmd, "--version"], capture_output=True)
        MAXIMA_OK = True
    except:
        try:
            subprocess.run(["sage", "-maxima", "--version"], capture_output=True)
            MAXIMA_OK = True
            MAXIMA_CMD_DEFAULT = ["sage", "-maxima"]
        except:
            pass

def run_maxima(batch_str, cmd=None):
    assert MAXIMA_OK
    if cmd is None:
        cmd = MAXIMA_CMD_DEFAULT

    cmdline = [*cmd, "-r", batch_str]
    #print(f"cmdline = {cmdline}")
    proc = subprocess.run(cmdline, capture_output=True)
    #print(proc)

def integrate_maxima(integ, cmd=None):
    #print(f"integ={integ}")
    bounds = []
    poly = integ
    while isinstance(poly, Int):
        bounds.append( (poly.vlo, poly.vhi, poly.v) )
        poly = poly.poly

    bounds.reverse()

    lo, hi, v = bounds[0]
    batch = f"integrate({poly},{v},{lo},{hi});"
    step = 1
    for (lo, hi, v) in bounds[1:]:
        batch += f"integrate(%o{step},{v},{lo},{hi});"
        step += 1
    batch += f'stringout("result.mx", %o{step});'
    batch += "quit();\n"

    run_maxima(batch, cmd)

    with open("result.mx", 'r') as f:
        result = f.read()

    frac_res = parse_frac(result)
    return frac_res


def parse_frac(inp):
    num_digits = ""
    pos = 0
    for ch in inp:
        if '0' <= ch <= '9':
            num_digits += ch
        elif ch == '/':
            break
        elif ch in { ' ', '\n', '\t', '\r' }:
            pass
        pos += 1

    if num_digits == "":
        raise ValueError("No digits found for numerator")

    den_digits = ""
    for ch in inp[pos+1:]:
        if '0' <= ch <= '9':
            den_digits += ch
        elif ch in { ' ', '\n', '\t', '\r' }:
            pass
        else:
            break
    
    if den_digits == "":
        raise ValueError("No digits found for denominator")

    return Fraction(int(num_digits), int(den_digits))

def init_sage():
    assert(SAGE_OK)

def to_sage(x):
    if isinstance(x, Monomial):
        if x.is_scalar():
            return QQ(str(x.coef))
        else:
            assert(len(x.vs.vs) == 1)
            assert(x.vs.maxdeg == 1)
            return var(str(x))
    else:
        raise ValueError(f"{x} is not a scalar or variable")
    
def integrate_sage(formula):
    if isinstance(formula, Int):
        p = integrate_sage(formula.poly)
        x, lo, hi = to_sage(formula.v), to_sage(formula.vlo), to_sage(formula.vhi)
        return integrate(p, x, lo, hi)
    elif isinstance(formula, Prod):
        return prod(map(integrate_sage, formula.polys))
    elif isinstance(formula, Monomial):
        return to_sage(formula)
    else:
        return formula

import os
path = os.path.dirname(__file__)

FLINTEGRATOR_CMD_DEFAULT = [path+"/../integrator/integrator"]
FLINTEGRATOR_OK = False
def init_flintegrator(cmd=None):
    global FLINTEGRATOR_OK, FLINTEGRATOR_CMD_DEFAULT
    if cmd is None:
        cmd = FLINTEGRATOR_CMD_DEFAULT
        
    try:
        subprocess.run([*cmd, "--version"], capture_output=True)
        FLINTEGRATOR_OK = True
    except:
        pass

def integrate_flint(integ):
    global FLINTEGRATOR_OK, FLINTEGRATOR_CMD_DEFAULT
    assert FLINTEGRATOR_OK

    #print("integ={}".format(str(integ)))
    cmd = FLINTEGRATOR_CMD_DEFAULT

    proc = subprocess.run([*cmd, str(integ)], capture_output=True)
    lines = proc.stdout.split()

    lastline = str(lines[-1])
    frac_res = parse_frac(lastline)
    return frac_res

INTEGRATE_MODE = 'flint'

def check_integrate_mode(mode):
    global SAGE_OK, HSINTEGRATOR_OK, MAXIMA_OK, FLINTEGRATOR_OK

    if mode == 'flint':
        if not FLINTEGRATOR_OK:
            raise Exception("Flint integrator not available")
    elif mode == 'maxima':
        if not MAXIMA_OK:
            raise Exception("Maxima not available")
    elif mode == 'sage':
        if not SAGE_OK:
            raise Exception("Sage not available")
    elif mode == 'hs':
        if not HSINTEGRATOR_OK:
            raise Exception("HS integrator not available")
    else:
        raise Exception("Unsupported integrator: {}".format(mode))

def set_integrate_mode(mode):
    global INTEGRATE_MODE
    check_integrate_mode(mode)
    INTEGRATE_MODE = mode

def init(mode=None):
    global INTEGRATE_MODE
    
    if mode is None or mode == 'flint':
        init_flintegrator()
    if mode is None or mode == 'maxima':
        init_maxima()
    if mode is None or mode == 'hs':
        init_hsintegrator()
    if mode is None or mode == 'sage':
        init_sage()

    if mode is not None:
        check_integrate_mode(mode)
        INTEGRATE_MODE = mode
    

def integrate(integ, mode=None):
    global INTEGRATE_MODE
    if mode is None:
        mode = INTEGRATE_MODE

    if mode == '' or mode is None:
        raise Exception("Integrator mode not set")

    if mode == 'flint':
        if not FLINTEGRATOR_OK:
            raise Exception("Flint integrator not available")
        return integrate_flint(integ)
    elif mode == 'maxima':
        if not MAXIMA_OK:
            raise Exception("Maxima not available")
        return integrate_maxima(integ)
    elif mode == 'hs':
        if not HSINTEGRATOR_OK:
            raise Exception("HS integrator not available")
        return integrate_hs(integ)
    elif mode == 'sage':
        if not SAGE_OK:
            raise Exception("Sage not available")
        return integrate_sage(integ)
    else:
        raise Exception("Unsupported integrator: {}".format(mode))


if __name__ == "__main__":
    print(f"Sage ok? {SAGE_OK}")

    init_hsintegrator()
    print(f"HS integrator ok? {HSINTEGRATOR_OK}")

    init_maxima()
    print(f"Maxima ok? {MAXIMA_OK}")

    init_flintegrator()
    print(f"Flintegrator ok? {FLINTEGRATOR_OK}")
