from formula import scalar, make_var
from poset import get_BITE_vertices
import log

def h(poset, without=None):
    return hash(tuple(x for x in poset.predecessors if x != without))

def minvar_strategy(poset):
    
    poset.transitive_reduction()
    b,i,t,e = get_BITE_vertices(poset)
    hp = h(poset)
    # memos : dict(hash : (poset, set(int), list[int]))
    # hash de poset -> poset, vars courantes, ordre d'élimination
    memos = {hp : (poset, set(), [])}
    
    todos = [list() for _ in range(len(poset))]
    todos[0] = [(x,hp) for x in b+i+t+e]
    min_var = 0

    if not (b+i+t+e):
        raise ValueError("We need split")
    
    while True:
        while todos[min_var]:
            # Récupération du candidat
            x, hp = todos[min_var].pop()
            poset, vars, old_elimination_order = memos[hp]
            new_hp = h(poset, without=x)

            # On s'en est déjà occupé
            if new_hp in memos:
                continue

            # mise à jour du nombres de variables
            new_vars = vars - {x}
            predx = poset.predecessors[x]
            succx = poset.successors(x)
            lo = scalar(0)
            hi = scalar(1)
            if predx:
                lo = make_var(list(predx)[0])
                new_vars.add(list(predx)[0])
            if succx:
                hi = make_var(list(succx)[0])
                new_vars.add(list(succx)[0])
                
            k = len(new_vars)
            elimination_order = old_elimination_order + [(x, lo, hi)]
            # On a fini ?
            if k == 0:
                log.SEARCH_DEPTH = min_var
                log.NB_VERTICES = len(memos)
                return elimination_order

            # On continue avec ce nouvel ordre
            poset = poset.deepcopy()
            poset.remove(x)
            poset.transitive_reduction()
            memos[new_hp] = poset, new_vars, elimination_order
            
            b,i,t,e = get_BITE_vertices(poset)
            if not (b+i+t+e):
                raise ValueError("We need split")

            # On prend le max pour considérer l'ordre d'élimination
            # avec le moins de variables
            k = max(k, min_var)
            for x in b+i+t+e:
                todos[k].append((x, new_hp))
        # end while
        min_var += 1
        
