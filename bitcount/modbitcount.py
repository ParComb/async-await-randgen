'''
Computation of the number of linear extension
of modular BIT-posets.
'''


import os
path = os.path.dirname(__file__)
path = "." if not path else path

import sys
sys.path.append(path+"/../lib/")
sys.path.append(path+"/../../mod-decomp/src/")

from sagetools import sage_available, count_le_sage

from posetio import load_poset

from moddecomp import modular_decomposition
from twostructure import TwoStructure, twostructure_from_dict
from poset import Poset, from_dict
from math import comb, factorial, prod
from formula import *
from formula_builder import build_formula_from_prime_graph, basic_strategy
from functools import reduce
from minvar_strategy import minvar_strategy

import log
import operator
import integrator

def reconstruct_prime_graph(qg, buckets):
    pg = TwoStructure()
    pg.modules = buckets
    for (u,v) in qg.colors[1]: #edges
        u_id = -1
        v_id = -1
        for i, b in enumerate(buckets):
            if qg.node_to_module(u) & b:
                u_id = i
            if qg.node_to_module(v) & b:
                v_id = i
        if u_id != v_id and u_id != -1 and v_id != -1:
            pg.edge(u_id, v_id)
    return pg

FORMULA_STRATEGY = basic_strategy

def count_le_tree(dtree):
    if dtree.node_type == 'LEAF' :
        return {dtree.node_id}, 1
    
    elif dtree.node_type == 'LINEAR':
        total_size, total_le_count = 0, 1
        node_ids = set()
        for ids, le_count in map(count_le_tree, dtree.children):
            node_ids |= ids
            total_le_count *= le_count
        return node_ids, total_le_count
    
    elif dtree.node_type == 'COMPLETE':
        total_le_count = 1
        node_ids = list()
        for ids, le_count in map(count_le_tree, dtree.children):
            node_ids.append(ids)
            total_le_count *= le_count
        sizes = list(map(len, node_ids))
        n = sum(sizes)
        total_le_count = factorial(n) // prod(map(factorial, sizes)) * total_le_count
        return reduce(operator.or_, node_ids), total_le_count
    
    elif dtree.node_type == 'PRIME':
        le_counts = list()
        node_ids = list()
        for ids, le_count in map(count_le_tree, dtree.children):
            node_ids.append(ids)
            le_counts.append(le_count)

        prime_graph = reconstruct_prime_graph(dtree.prime_graph, node_ids)
        formula = build_formula_from_prime_graph(prime_graph, strategy=FORMULA_STRATEGY)
        le_count_max = integrator.integrate(formula)

        total_size = sum(map(len, node_ids))
        total_le_count = factorial(total_size) * le_count_max * prod(le_counts)
            
        return reduce(operator.or_, node_ids), total_le_count
            
    else:
        raise TypeError(f"The vertex's type {dtree.node_type} is not supported")
        
def count_le(poset, strategy=basic_strategy, debug=False):
    global FORMULA_STRATEGY
    tc = poset.transitive_closure()
    ts = twostructure_from_dict(tc)
    dtree = modular_decomposition(ts)
    if debug:
        print("[debug] Modular decomposition")
        print(f"    ==> {dtree.to_decomp()}")
    FORMULA_STRATEGY = strategy
    _, le = count_le_tree(dtree)
    return le

def ex_diamond(nb_branches=3):
    p = Poset()
    for i in range(2+nb_branches):
        p.add(i)
    for i in range(nb_branches):
        p.add_rel(0,1+i)
        p.add_rel(1+i,nb_branches+1)
    return p

def ex_n_and_diam():
    p = ex_diamond()
    n = len(p.elems())
    for i in range(3):
        p.add(i+n)
    p.add_rel(n,0)
    p.add_rel(n,n+1)
    p.add_rel(n+2,n+1)
    return p

def poset_to_fix1():
    d = {2: [18, 19], 5: [19], 8: [18], 13: [24], 15: [], 18: [24], 19: [24], 24: [], 25: [2, 5, 13, 15]}
    return from_dict(d)


def local_tests():
    # p = ex_diamond()
    print("Diamond+N:")
    p = ex_n_and_diam()
    if sage_available():
        print(f"Sage count {count_le_sage(p)} le.")
    n = count_le(p, debug=False)
    print(f"The number of le is : {n}")

    import posetex

    print("\nPoset 1:")
    poset1 = posetex.make_poset1()
    if sage_available():
        print(f"Sage count {count_le_sage(poset1)} le.")
    n1 = count_le(poset1, debug=False)
    print(f"#le(poset1) = {n1}")

    print("\nPoset 2:")
    poset2 = posetex.make_poset2()
    # if sage_available():
    #     print(f"Sage count {count_le_sage(poset2)} le.")
    n2 = count_le(poset2, debug=False)
    print(f"#le(poset2) = {n2}")

    print("\nPoset 3:")
    poset3 = posetex.make_poset3()
    if sage_available():
        print(f"Sage count {count_le_sage(poset3)} le.")
    n3 = count_le(poset3, debug=False)
    print(f"#le(poset3) = {n3}")

    print("\nMips poset:")
    mips_poset = posetex.make_mips_poset()
    if sage_available():
        print(f"Sage count {count_le_sage(mips_poset)} le.")
    nm = count_le(mips_poset, debug=False)
    print(f"#le(mips_poset) = {nm}")

    print("\nPoset 4:")
    poset4 = posetex.make_poset4()
    if sage_available():
        print(f"Sage count {count_le_sage(poset4)} le.")
    n4 = count_le(poset4, debug=False)
    print(f"#le(poset4) = {n4}")

    print("\nPoset 5:")
    poset5 = posetex.make_poset5()
    if sage_available():
        print(f"Sage count {count_le_sage(poset5)} le.")
    n5 = count_le(poset5, debug=False)
    print(f"#le(poset5) = {n5}")

    print("\nPoset 6:")
    poset6 = posetex.make_poset6()
    if sage_available():
        print(f"Sage count {count_le_sage(poset6)} le.")
    n6 = count_le(poset6, debug=False)
    print(f"#le(poset6) = {n6}")

    print("\nPoset 7:")
    poset7 = poset_to_fix1()
    if sage_available():
        print(f"Sage count {count_le_sage(poset7)} le.")
    n7 = count_le(poset7, debug=False)
    print(f"#le(poset7) = {n7}")
    
def error(msg):
    import sys
    sys.exit(msg)
    

if __name__ == '__main__':

    import argparse

    cmdparser = argparse.ArgumentParser(description="Counting linear extensions of modular BIT-partial orders.")
    cmdparser.add_argument('file', metavar='<file>', type=str, help='the name of the file containing the Poset description', default=None, nargs='?')
    cmdparser.add_argument('-m', '--maxima', action='store_true', help='use maxima backend', default=False)
    cmdparser.add_argument('-f', '--flint', action='store_true', help='use flintegrator backend (default)', default=True)
    cmdparser.add_argument('--selftest', action='store_true', help='perform self-tests', default=False)
    cmdparser.add_argument('--quiet', action='store_true', help='quiet mode, only show the linear extension count', default=False)
    cmdparser.add_argument('--strategy', choices=['basic', 'minvar'], default='basic', help='the integral formula building strategy')
    cmdparser.add_argument('--debug', action='store_true', help='show debug informations', default=False)
                                        
    cmdargs = cmdparser.parse_args()
    # print(cmdargs)

    if not cmdargs.quiet:
        print("Modbitcount v0.3 - Copyright (C) 2021 The ParCo project")
        print("================")

    if cmdargs.file is None and cmdargs.selftest != True:
        print("Missing <file> argument")
        cmdparser.print_usage()
        error("Abort.")

    int_mode = 'flint'
    if cmdargs.maxima:
        int_mode = 'maxima'
    if not cmdargs.quiet:
        print(f"> Integrator: {int_mode}")
    
    integrator.init(int_mode)

    if cmdargs.selftest:
        local_tests()
        sys.exit()

    filename = cmdargs.file
    
    posetdic = load_poset(filename, mode='matrix')
    poset = Poset(dico=posetdic)

    strategy = basic_strategy
    if cmdargs.strategy == 'minvar':
        strategy = minvar_strategy
    if not cmdargs.quiet:
        print(f"> Strategy: {cmdargs.strategy}")
        print("----")

    debug = False
    if cmdargs.debug and not cmdargs.quiet:
        debug = True

    nb = count_le(poset, strategy=strategy, debug=debug)
    
    if not cmdargs.quiet:
        print(f"{filename}: ", end='')
        print("#le = ", end='')
    print(nb)

