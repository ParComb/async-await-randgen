'''
Sampling of linear extension
of modular BIT-posets.
'''


import os
path = os.path.dirname(__file__)
path = "." if not path else path

import sys
sys.path.append(path+"/../lib/")
sys.path.append(path+"/../../mod-decomp/src/")

import subprocess

from posetio import load_poset

from moddecomp import modular_decomposition
from twostructure import TwoStructure, twostructure_from_dict
from poset import Poset, from_dict
from math import comb, factorial, prod
from formula import *
from formula_builder import build_formula_from_prime_graph, basic_strategy
from functools import reduce
from minvar_strategy import minvar_strategy

import log
import operator
import integrator
import random

def reconstruct_prime_graph(qg, buckets):
    #import pdb ; pdb.set_trace()
    pg = TwoStructure()
    pg.modules = buckets
    for (u,v) in qg.colors[1]: #edges
        u_id = -1
        v_id = -1
        for i, b in enumerate(buckets):
            if qg.node_to_module(u) & set(b):
                u_id = i
            if qg.node_to_module(v) & set(b):
                v_id = i
        if u_id != v_id and u_id != -1 and v_id != -1:
            pg.edge(u_id, v_id)
    return pg

FORMULA_STRATEGY = basic_strategy

def shuffle_lin_exts(lin_exts):
    lin_ext = []
    total_size = sum(map(len, lin_exts))
    for k in range(total_size):
        i = random.randrange(total_size - k)
        j = 0
        while i >= len(lin_exts[j]):
            i -= len(lin_exts[j])
            j += 1
        lin_ext.append(lin_exts[j].pop(0))
        if lin_exts[j] == []:
            del lin_exts[j]
    return lin_ext


def flint_sample(formula):
    print(formula)
    
    sampler_cmd = path+"/../integrator/sampler"
    # print(str(formula))
    proc = subprocess.run([sampler_cmd, str(formula)], capture_output=True)
    # print(proc.stdout.decode('utf-8'))
    lines = proc.stdout[:-1].split(b"\n")
    values = []
    for line in lines[::-1]:
        # print(line)
        if line.startswith(b"="):
            break
        values.append(float(line.split(b" ")[0]))
    values = values[::-1]
    lin_ext = [(i, v) for (i,v) in enumerate(values) if v != 0.]
    lin_ext = sorted(lin_ext, key=lambda x: x[1])
    return list(map(lambda x: x[0], lin_ext))
    
    
def sample_le_tree(dtree):
    if dtree.node_type == 'LEAF' :
        return [dtree.node_id]
    
    elif dtree.node_type == 'LINEAR':
        return [elem for lin_ext in map(sample_le_tree, dtree.children) for elem in lin_ext]
    elif dtree.node_type == 'COMPLETE':
        return shuffle_lin_exts(list(map(sample_le_tree, dtree.children)))
    elif dtree.node_type == 'PRIME':
        node_ids = list(map(sample_le_tree, dtree.children))
        prime_graph = reconstruct_prime_graph(dtree.prime_graph, node_ids)
        formula = build_formula_from_prime_graph(prime_graph, strategy=FORMULA_STRATEGY)
        lin_ext = flint_sample(formula)
        return lin_ext
    else:
        raise TypeError(f"The vertex's type {dtree.node_type} is not supported")
        
def sample_le(poset, strategy=basic_strategy, debug=False):
    global FORMULA_STRATEGY
    tc = poset.transitive_closure()
    ts = twostructure_from_dict(tc)
    dtree = modular_decomposition(ts)
    if debug:
        print("[debug] Modular decomposition")
        print(f"    ==> {dtree.to_decomp()}")
    FORMULA_STRATEGY = strategy
    le = sample_le_tree(dtree)
    return le

def ex_diamond(nb_branches=3):
    p = Poset()
    for i in range(2+nb_branches):
        p.add(i)
    for i in range(nb_branches):
        p.add_rel(0,1+i)
        p.add_rel(1+i,nb_branches+1)
    return p

def ex_n_and_diam():
    p = ex_diamond()
    n = len(p.elems())
    for i in range(3):
        p.add(i+n)
    p.add_rel(n,0)
    p.add_rel(n,n+1)
    p.add_rel(n+2,n+1)
    return p

def poset_to_fix1():
    d = {2: [18, 19], 5: [19], 8: [18], 13: [24], 15: [], 18: [24], 19: [24], 24: [], 25: [2, 5, 13, 15]}
    return from_dict(d)


def local_tests():
    # p = ex_diamond()
    print("Diamond+N:")
    p = ex_n_and_diam()
    n = sample_le(p, debug=False)
    print(n)

    import posetex

    print("\nPoset 1:")
    poset1 = posetex.make_poset1()
    n1 = sample_le(poset1, debug=False)
    print(f"#le(poset1) = {n1}")

    print("\nPoset 2:")
    poset2 = posetex.make_poset2()
    n2 = sample_le(poset2, debug=False)
    print(f"#le(poset2) = {n2}")

    print("\nPoset 3:")
    poset3 = posetex.make_poset3()
    n3 = sample_le(poset3, debug=False)
    print(f"#le(poset3) = {n3}")

    print("\nMips poset:")
    mips_poset = posetex.make_mips_poset()
    nm = sample_le(mips_poset, debug=False)
    print(f"#le(mips_poset) = {nm}")

    print("\nPoset 4:")
    poset4 = posetex.make_poset4()
    n4 = sample_le(poset4, debug=False)
    print(f"#le(poset4) = {n4}")

    print("\nPoset 5:")
    poset5 = posetex.make_poset5()
    n5 = sample_le(poset5, debug=False)
    print(f"#le(poset5) = {n5}")

    print("\nPoset 6:")
    poset6 = posetex.make_poset6()
    n6 = sample_le(poset6, debug=False)
    print(f"#le(poset6) = {n6}")

    print("\nPoset 7:")
    poset7 = poset_to_fix1()
    n7 = sample_le(poset7, debug=False)
    print(f"#le(poset7) = {n7}")
    
def error(msg):
    import sys
    sys.exit(msg)
    

if __name__ == '__main__':

    import argparse

    cmdparser = argparse.ArgumentParser(description="Sampling linear extensions of modular BIT-partial orders.")
    cmdparser.add_argument('file', metavar='<file>', type=str, help='the name of the file containing the Poset description', default=None, nargs='?')
    cmdparser.add_argument('--selftest', action='store_true', help='perform self-tests', default=False)
    cmdparser.add_argument('--quiet', action='store_true', help='quiet mode', default=False)
    cmdparser.add_argument('--strategy', choices=['basic', 'minvar'], default='basic', help='the integral formula building strategy')
    cmdparser.add_argument('--debug', action='store_true', help='show debug informations', default=False)
                                        
    cmdargs = cmdparser.parse_args()
    # print(cmdargs)

    if not cmdargs.quiet:
        print("Modbitsample v0.3 - Copyright (C) 2021 The ParCo project")
        print("================")

    if cmdargs.file is None and cmdargs.selftest != True:
        print("Missing <file> argument")
        cmdparser.print_usage()
        error("Abort.")

    if cmdargs.selftest:
        local_tests()
        sys.exit()

    filename = cmdargs.file
    
    posetdic = load_poset(filename, mode='matrix')
    poset = Poset(dico=posetdic)

    strategy = basic_strategy
    if cmdargs.strategy == 'minvar':
        strategy = minvar_strategy
    if not cmdargs.quiet:
        print(f"> Strategy: {cmdargs.strategy}")
        print("----")

    debug = False
    if cmdargs.debug and not cmdargs.quiet:
        debug = True

    linext = sample_le(poset, strategy=strategy, debug=debug)
    
    if not cmdargs.quiet:
        print(f"{filename}: ", end='')
        print("linext = ", end='')
    print(linext)

