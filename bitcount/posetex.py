from poset import Poset

# POSET examples

def make_poset1():
    poset1 = Poset()
    poset1.add(0)
    poset1.add(1)
    poset1.add(2)
    poset1.add(3)
    poset1.add(4)
    poset1.add(5)
    poset1.add(6)
    poset1.add_rel(0, 1)
    poset1.add_rel(1, 2)
    poset1.add_rel(1, 4)
    poset1.add_rel(2, 3)
    poset1.add_rel(4, 5)
    poset1.add_rel(5, 3)
    poset1.add_rel(3, 6)

    return poset1

def make_poset2():
    poset2 = Poset()

    nodes = [poset2.add(i) for i in range(1,16)]
    
    def edge(i,j):
        poset2.add_rel(i, j)
        
    edge(1,2)
    edge(1,11)
    edge(2,3)
    edge(2,6)
    edge(2,7)
    edge(3,4)
    edge(3,5)
    edge(7,8)
    edge(7,9)
    edge(9,10)
    edge(11,12)
    edge(11,13)
    edge(13,14)
    edge(13,15)
    edge(11,10)
    
    return poset2

def make_poset3():
    poset3 = Poset()

    nodes = [poset3.add(i) for i in range(1,10)]
    
    def edge(i,j):
        poset3.add_rel(i, j)
        
    edge(1,2)
    edge(1,3)
    edge(2,4)
    edge(2,5)
    edge(3,5)
    edge(3,6)
    edge(4,7)
    edge(5,7)
    edge(5,8)
    edge(6,8)
    edge(7,9)
    edge(8,9)
    
    
    return poset3

def make_mips_poset():
    poset = Poset()
    
    nodes = [poset.add(i) for i in range(1,11)]
    
    def edge(i,j):
        poset.add_rel(i, j)

    edge(1, 2)
    edge(1, 3)
    edge(3, 4)
    edge(3, 5)
    edge(5, 6)
    edge(6, 7)
    edge(4, 7)
    edge(7, 8)
    edge(8, 9)
    edge(2, 10)
    edge(7, 10)
    #edge(10, 11)
    #edge(9, 11)

    return poset
        
def make_poset4():
    poset4 = Poset()

    nodes = [poset4.add(i) for i in range(1,19)]
    
    def edge(i,j):
        poset4.add_rel(i,j)
        
    edge(1,2)
    edge(2,3)
    edge(3,4)
    edge(4,5)

    edge(1,6)
    edge(6,7)
    edge(7,8)

    edge(8,9)

    #edge(9,10)
    #edge(10,11)
    #edge(11,12)
    #edge(12,5)

    edge(9,10)
    edge(9,11)
    edge(11,12)
    edge(10,12)
    edge(12,5)

    edge(5,13)
    edge(13,14)
    edge(14,15)
    edge(15,18)

    edge(8,16)
    edge(16,17)
    edge(17,18)

    return poset4

def make_poset5():
    poset4 = Poset()

    nodes = [poset4.add(i) for i in range(1,12)]
    
    def edge(i,j):
        poset4.add_rel(i,j)
        
    edge(1,2)
    edge(2,3)

    edge(1,4)

    edge(4,5)

    

    edge(5,6)
    edge(5,7)
    edge(7,8)
    edge(6,8)
    edge(8,3)

    edge(3,11)
    edge(11,10)

    edge(4,9)
    edge(9,10)
    
    return poset4

def make_poset6():
    poset4 = Poset()

    nodes = [poset4.add(i) for i in range(1,11)]
    
    def edge(i,j):
        poset4.add_rel(i,j)
        
    edge(1,2)
    edge(2,3)

    edge(1,4)

    edge(4,5)

    

    edge(5,6)
    edge(5,7)
    edge(7,8)
    edge(6,8)
    edge(8,3)

    edge(3,10)

    edge(4,9)
    edge(9,10)
    
    return poset4


def make_spt_poset1(n):
    poset = Poset()
    PosetVertex.NODE_ID = int(0)

    root_node = poset.add()
    pred_node = root_node

    for k in range(1, n+1):
        next_node = poset.add()
        poset.add_rel(pred_node, next_node)
        pred_node = next_node

    middle_node = poset.add()
    poset.add_rel(root_node, middle_node)
    poset.add_rel(root_node, pred_node)
    
    node1 = poset.add()
    poset.add_rel(middle_node, node1)
    node2 = poset.add()
    poset.add_rel(node1, node2)
    end_node = poset.add()
    poset.add_rel(node2, end_node)
    poset.add_rel(pred_node, end_node)

    return poset

def make_crown_2_2_3():
    poset = Poset()
    PosetVertex.NODE_ID = int(0)

    a = poset.add()
    b = poset.add()
    c = poset.add()
    d = poset.add()
    e = poset.add()
    f = poset.add()

    ac = poset.add()
    bc = poset.add()
    abc = poset.add()

    poset.add_rel(a, ac)
    poset.add_rel(c, ac)
    poset.add_rel(c, bc)
    poset.add_rel(b, bc)
    poset.add_rel(ac, d)
    poset.add_rel(ac, abc)
    poset.add_rel(abc, e)
    poset.add_rel(bc, abc)
    poset.add_rel(bc, f)

    return poset

def make_spt_poset2(n,m):
    poset = Poset()
    PosetVertex.NODE_ID = int(0)

    root_node = poset.add()
    pred_node = root_node

    for k in range(1, n+2):
        next_node = poset.add()
        poset.add_rel(pred_node, next_node)
        pred_node = next_node

    middle_node = poset.add()
    poset.add_rel(root_node, middle_node)
    poset.add_rel(root_node, pred_node)
    
    pred_node1 = middle_node
    for k in range(1, m):
        next_node1 = poset.add()
        poset.add_rel(pred_node1, next_node1)
        pred_node1 = next_node1

    end_node = poset.add()
    poset.add_rel(pred_node1, end_node)
    poset.add_rel(pred_node, end_node)

    return poset

def make_linear(poset, start_node, end_node, nb_nodes):
    pred_node = start_node
    nodes = [start_node]
    for k in range(1, nb_nodes + 1):
        next_node = poset.add()
        nodes.append(next_node)
        poset.add_rel(pred_node, next_node)
        pred_node = next_node

    poset.add_rel(pred_node, end_node)
    nodes.append(end_node)

    return nodes

# petrinets 19 paper: 1975974
def make_pn19_poset():
    poset = Poset()
    PosetVertex.NODE_ID = int(0)

    init = poset.add()
    
    gen = poset.add()
    yield1 = poset.add()
    yield2 = poset.add()

    load = poset.add()
    xform = poset.add()
    
    step1 = poset.add()
    step2 = poset.add()
    step3 = poset.add()
    step4 = poset.add()
    end = poset.add()
    
    fork = poset.add()
    comp1 = poset.add()
    comp2_1 = poset.add()
    comp2_2 = poset.add()
    join = poset.add()

    poset.add_rel(init, gen)
    poset.add_rel(init, step1)
    poset.add_rel(init, fork)

    poset.add_rel(gen, yield1)
    poset.add_rel(yield1, yield2)
    poset.add_rel(yield1, step3)
    poset.add_rel(yield2, end)

    poset.add_rel(step1, load)
    poset.add_rel(load, xform)
    poset.add_rel(xform, step4)

    poset.add_rel(step1, step2)
    poset.add_rel(step2, step3)
    poset.add_rel(step3, step4)
    poset.add_rel(step4, end)

    poset.add_rel(fork, comp1)
    poset.add_rel(comp1, join)
    poset.add_rel(fork, comp2_1)
    poset.add_rel(comp2_1, comp2_2)
    poset.add_rel(comp2_2, join)
    poset.add_rel(join, end)

    return poset

def make_arch_poset(n, k):
    poset = Poset()
    xi =  [ poset.add() for _ in range(k)]
    ai =  [ poset.add() for _ in range(n)]
    bi =  [ poset.add() for _ in range(n)]
    ci =  [ poset.add() for _ in range(n)]

    for i in range(n):
        poset.add_rel(ai[i],ci[i])
        poset.add_rel(ci[i],bi[i])

    for i in range(n-1):
        poset.add_rel(ai[i],ai[i+1])
        poset.add_rel(bi[i],bi[i+1])

    for i in range(k-1):
        poset.add_rel(xi[i],xi[i+1])

    poset.add_rel(ai[n-1],xi[0])
    poset.add_rel(xi[k-1],bi[0])

    return poset
    
    

def make_spt_posetK(n, k):
    poset = Poset()
    
    root_node = poset.add()
    bot_node = poset.add()
    
    nodes = make_linear(poset, root_node, bot_node, k + k - 2)

    for i in range(0, k-1):
        make_linear(poset, nodes[i], nodes[i+k], n)

    return poset

