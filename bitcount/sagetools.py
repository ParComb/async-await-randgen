


SAGE_OK = False
try:
    from sage.all import Poset as SagePoset
    SAGE_OK = True
except:
    pass

def sage_available():
    global SAGE_OK
    return SAGE_OK

def count_le_sage(poset):
    p = SagePoset(poset.to_dict())
    return len(list(p.linear_extensions()))



