"""
Recognizing BIT posets  (more or less) efficiently
"""
###
###
### Load posets with two formats:
###
### 1) successors map
### - lines starting with a hashtag # are not taken into account (comments)
### - each line is a set of space-separateds *distinct* positive integers
### - the line number is the current node
### - the positive numbers are its direct successors
###
### 2) adjascency 0-1 matrix
### - In line i column j : a 1 means i > j

class Poset:
    def __init__(self, dic=None):
        if dic is None:
            self.nodes = []
        else:
            self.nodes = build_poset_from_dict(dic).nodes
        
    def size(self):
        return len(self.nodes)
        
    def add_node(self):
        self.nodes.append( (set(), set()) )
        return len(self.nodes) - 1

    def add_edge(self, src, dst):
        assert 0 <= src < len(self.nodes)
        assert 0 <= dst < len(self.nodes)
        _, succs = self.nodes[src]
        self.successors(src).add(dst)
        self.predecessors(dst).add(src)

    def remove_node(self, node):
        preds, succs = self.nodes[node]
        self.nodes[node] = (set(), set())
        if preds:
            if succs:
                for pred in list(preds):
                    self.successors(pred).remove(node)
                    for succ in list(succs):
                        self.predecessors(succ).remove(node)
                        if not self.has_path(pred, succ):
                            self.add_edge(pred, succ)
            else:
                for pred in list(preds):
                    self.successors(pred).remove(node)
        else:
            for succ in list(succs):
                self.predecessors(succ).remove(node)
        
    def remove_edge(self, src, dst):
        self.successors(src).remove(dst)
        self.predecessors(dst).remove(src)

    def successors(self, node):
        _, succs = self.nodes[node]
        return succs

    def predecessors(self, node):
        preds, _ = self.nodes[node]
        return preds

    def has_no_edge(self):
        for (preds, succs) in self.nodes:
            if preds or succs:
                return False
        return True

    def to_dict(self, from_pred=False):
        dico = dict()
        for node, (preds, succs) in enumerate(self.nodes):
            dests = preds if from_pred else succs
            dico[node] = dests
        return dico

    def check_invariant(self):
        for node, (preds, succs) in enumerate(self.nodes):
            for pred in preds:
                if node not in self.successors(pred):
                    raise ValueError(f"Missing node '{node}' as successor of predecessor '{pred}'")
            for succ in succs:
                if node not in self.predecessors(succ):
                    raise ValueError(f"Missing node '{node}' as predecessor of successor '{succ}'")
        return True

def build_poset_from_dict(dic):
    #print(f"[build]: dic={dic}")
    pos = Poset()
    srcs = dict()
    for src in dic:
        nsrc = pos.add_node()
        srcs[src] = nsrc
    for src, dsts in dic.items():
        for dst in dsts:
            if dst not in srcs:
                ndst = pos.add_node()
                srcs[dst] = ndst
            pos.add_edge(srcs[src], srcs[dst])
    return pos

def slurp_file(f):
    lines = []
    
    while True:
        line = f.readline()
        if line == "":
            return lines
        if not line.lstrip().startswith("#"):
            lines.append([int(e) for e in line.split()])

def load_poset(fname):
    with open(fname, 'r') as f:
        nodes = slurp_file(f)
    if len(nodes) == 0:
        return Poset()

    ### dummy heuristics
    nums = nodes[len(nodes) // 2]

    is_matrix = True
    for num in nums:
        if num != 0 and num != 1:
            is_matrix = False

    if is_matrix:
        #print(f"Building poset '{fname}' (matrix mode)") 
        return build_matrix(nodes)
    else:
        #print(f"Building poset '{fname}' (successors mode)") 
        return build_successors(nodes)

def build_successors(nodes):
    p = Poset()

    # build nodes
    for _ in nodes:
        p.add_node()

    # build edges
    src = 0
    for succs in nodes:
        for dst in succs:
            p.add_edge(src, dst)
        src += 1

    return p

def build_matrix(nodes):
    p = Poset()

    # build nodes
    for _ in nodes:
        p.add_node()

    # build edges
    for src, line in enumerate(nodes):
        for dst, val in enumerate(line):
            if val == 1:
                p.add_edge(src, dst)
                
    return p

def poset_has_path(poset, src, dst):
    stk = [src]
    while stk:
        src = stk.pop()
        succs = poset.successors(src)
        if dst in succs:
            return True
        stk.extend(succs)
    return False

Poset.has_path = poset_has_path

def edge_is_transitive(poset, src, dst):
    succs = poset.successors(src)
    for src2 in succs:
        if poset.has_path(src2, dst):
            return True
    return False

def poset_is_intransitive(poset):
    for src in range(0, poset.size()):
        #print("============")
        #print(f"src = {src}")
        succs = poset.successors(src)
        for dst in succs:
            #print(f"dst = {dst}")
            if edge_is_transitive(poset, src, dst):
                # print(f" ==> Transitive edge: {src} -> {dst}")
                return False
    return True

Poset.is_intransitive = poset_is_intransitive

def transitive_reduction(poset):
    for src in range(0, poset.size()):
        for dst in [e for e in poset.successors(src)]:
            if edge_is_transitive(poset, src, dst):
                poset.remove_edge(src, dst)

Poset.transitive_reduction = transitive_reduction

def fetch_BIT_nodes(poset):
    bits = set()
    for node, (preds, succs) in enumerate(poset.nodes):
        if (len(preds) == 1 and len(succs) in {0,1}) or (len(preds) == 0 and len(succs) == 1):
            bits.add(node)
    return bits

def BIT_decomposition(poset):
    poset.check_invariant()
    while True:
        bits = fetch_BIT_nodes(poset)
        if bits:
            for node in bits:
                #print(f"node={node}")
                poset.check_invariant()
                #import pdb ; pdb.set_trace()
                poset.remove_node(node)
                poset.check_invariant()
        else:
            return poset    
    
def test_if_BIT(p):
    if p.is_intransitive():
        # print("  ==> Poset is intransitive (OK)")
        pass
    else:
        # print("  ==> Poset is transitive")
        # print("      ... removing transitive edges")
        transitive_reduction(p)
        if p.is_intransitive():
            # print("      ... Poset is *now* intransitive (OK)")
            pass
        else:
            sys.abort("Failed transitive reduction (please report)")

    BIT_decomposition(p)
    if p.has_no_edge():
        return True, p
    else:
        return False, p
            
if __name__ == "__main__":
    import sys
    for fname in sys.argv[1:]:
        #print("================================")
        p = load_poset(fname)
        #print(f"poset loaded ({p.size()} nodes)")
        #print(p.nodes)

        bit, rest = test_if_BIT(p)

        if bit:
            print(f"[BIT]: {fname}")
        else:
            print(f"[SPLIT]: {fname}")
            

