'''
Classifying Partial orders
'''

import sys
sys.path.append("../lib/")
sys.path.append("../../mod-decomp/src/")

import poset as po
import posetio as pio

import moddecomp as md
from twostructure import twostructure_from_dict

import bitrec

def classify_po(poset, debug=False):
    tc = poset.transitive_closure()
    ts = twostructure_from_dict(tc)
    dtree = md.modular_decomposition(ts)
    if debug:
        print(dtree.to_decomp())
    category, splits  = classify_po_from_decomp(dtree, debug)
    return category, splits

def classify_po_from_decomp(dtree, debug=False):
    #import pdb ; pdb.set_trace()
    if dtree.node_type == 'LEAF' :
        return 'SP', []
    
    elif dtree.node_type in {'LINEAR', 'COMPLETE'}:
        all_child_splits = []
        node_cat = 'SP'
        for child_cat, child_splits in map(classify_po_from_decomp, dtree.children):
            all_child_splits.extend(child_splits)
            if child_cat == 'SPLIT':
                node_cat = 'SPLIT'
            elif child_cat == 'BIT' and node_cat == 'SP':
                node_cat = 'BIT'
        return node_cat, all_child_splits
    
    elif dtree.node_type == 'PRIME':
        node_dict = md.reconstruct_prime_graph_dict(dtree)
        node_po = bitrec.build_poset_from_dict(node_dict)
        isbit, _ = bitrec.test_if_BIT(node_po)
        if isbit:
            node_cat = 'BIT'
            all_splits = []
        else:
            node_cat = 'SPLIT'
            all_splits = [node_dict]

        for child_cat, child_splits in map(classify_po_from_decomp, dtree.children):
            all_splits.extend(child_splits)
            if child_cat == 'SPLIT':
                node_cat = 'SPLIT'
        return node_cat, all_splits
    else:
        raise TypeError(f"The vertex's type {dtree.node_type} is not supported")


if __name__ == "__main__":
    import sys
    for fname in sys.argv[1:]:
        #print("================================")
        p = pio.load_poset(fname)
        #print(f"poset loaded ({p.size()} nodes)")
        #print(p.nodes)

        poset = po.Poset(dico=p)
        pocat, psplits = classify_po(poset)

        print(f"[{pocat}]: {fname}")

