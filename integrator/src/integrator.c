
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <flint/fmpq_mpoly.h>
#include "parse_integral.h"

#define VDEBUG 0


/* Be careful : global variables */
fmpq_mpoly_ctx_t ctx;   // flint context
char ** varNames;       // Storage of variable names (for debugging)
int nbVars;             // number of variables

Bound zero = { ZERO, 0};
Bound one = { ONE, 1};

void init(int nb_vars) {
  // 1. initialization of symbolic variables
  varNames = (char**) malloc(sizeof(char *) * nb_vars);
  nbVars = nb_vars;
  for(int k=0; k<nb_vars; k++) {
    char temp_str[64];
    int nb = snprintf(temp_str, 64, "x%d", k);
    if (nb < 0) {
      err("Cannot build variable (please report)");
    }
      
    varNames[k] = (char *) malloc(sizeof(char) * (nb+1));
    strcpy(varNames[k], temp_str);
  }

  // 2. initialization of polynomials
  fmpq_mpoly_ctx_init(ctx, nb_vars, ORD_LEX);
}

void cleanup() {
  fmpq_mpoly_ctx_clear(ctx);

  // 1. Cleanup variables
  for(int k=0; k<nbVars; k++) {
    free(varNames[k]);
    varNames[k] = (char *) NULL;
  }
  free(varNames);
  varNames = (char **) NULL;
}

fmpq_mpoly_struct evaluate(fmpq_mpoly_t poly, int var_ref, BoundKind bk) {
  fmpq_t cons;
  fmpq_init(cons);
  switch(bk) {
  case ZERO:
    fmpq_zero(cons);
    break;
  case ONE:
    fmpq_one(cons);
    break;
    default:
      err("Wrong bound kind (please report)");
  }
  fmpq_mpoly_t eval;
  fmpq_mpoly_init(eval, ctx);
  fmpq_mpoly_evaluate_one_fmpq(eval, poly, var_ref, cons, ctx);
  return eval[0];
}

fmpq_mpoly_struct substitute(fmpq_mpoly_t poly, int from_var, int to_var) {
  slong * substs = (slong *) malloc(sizeof(slong) * nbVars);
  for(int var_ref = 0; var_ref < nbVars; var_ref++) {
    if (var_ref == from_var) {
      substs[var_ref] = to_var;
    } else {
      substs[var_ref] = var_ref;
    }
  }
  /* printf("\n["); */
  /* for(int i=0; i<nbVars;i++) { */
  /*   printf("%ld,", substs[i]); */
  /* } */
  /* printf("]\n"); */
  fmpq_mpoly_t subst;
  fmpq_mpoly_init(subst, ctx);
  fmpq_mpoly_compose_fmpq_mpoly_gen(subst, poly, (const slong *) substs, ctx, ctx);
  free(substs);
  return subst[0];
}

fmpq_mpoly_struct integrate(int step, fmpq_mpoly_t poly, int var_ref, Bound from, Bound to) {
  printf("Step %d\n", step);
  if (VDEBUG) {
    printf("------\n");
    printf("Integrate: ");
    fmpq_mpoly_print_pretty(poly, (const char**) varNames, ctx);
    printf(" from "); print_bound(from);
    printf(" to "); print_bound(to);
    printf(" for variable "); print_var(var_ref);
    printf("\n");
  }

  fmpq_mpoly_t prim;
  fmpq_mpoly_init(prim, ctx);
  fmpq_mpoly_integral(prim, poly, var_ref, ctx);
  if (VDEBUG) {
    printf(" ==> primitive = ");
    fmpq_mpoly_print_pretty(prim, (const char**) varNames, ctx);
  }
  
  fmpq_mpoly_t to_subst;
  if(to.kind == VAR) {
    to_subst[0] = substitute(prim, var_ref, to.val);
  } else {
    to_subst[0] = evaluate(prim, var_ref, to.kind);
  }
  if (VDEBUG) {
    printf("\n ==> to_subst = ");
    fmpq_mpoly_print_pretty(to_subst, (const char**) varNames, ctx);
  }

  fmpq_mpoly_t from_subst;
  if(from.kind == VAR) {
    from_subst[0] = substitute(prim, var_ref, from.val);
  } else {
    from_subst[0] = evaluate(prim, var_ref, from.kind);
  }

  if (VDEBUG) {
    printf("\n ==> from_subst = ");
    fmpq_mpoly_print_pretty(from_subst, (const char**) varNames, ctx);
    printf("\n");
  }

  fmpq_mpoly_t result;
  fmpq_mpoly_init(result, ctx);
  fmpq_mpoly_sub(result, to_subst, from_subst, ctx);

  if (VDEBUG) {
    printf("Result: ");
    fmpq_mpoly_print_pretty(result, (const char**) varNames, ctx);
    printf("\n");
  }

  return result[0];
}


Bound x(int var_ref) {
  return (Bound) { VAR, var_ref };
}

fmpq do_example_integral() {
  init(10);
  fmpq_mpoly_t p_one;
  fmpq_mpoly_init(p_one, ctx);
  fmpq_mpoly_one(p_one, ctx);

  fmpq_mpoly_t step1;
  step1[0] = integrate(1, p_one, /*dx*/5, x(0), one);
  fmpq_mpoly_t step2;
  step2[0] = integrate(2, step1, /*dx*/7, zero, x(0));
  fmpq_mpoly_t step3;
  step3[0] = integrate(3, step2, /*dx*/0, x(3), one);
  fmpq_mpoly_t step4;
  step4[0] = integrate(4, step3, /*dx*/3, x(1), one);
  fmpq_mpoly_t step5;
  step5[0] = integrate(5, step4, /*dx*/4, zero, x(1));
  fmpq_mpoly_t step6;
  step6[0] = integrate(6, step5, /*dx*/8, x(1), one);
  fmpq_mpoly_t step7;
  step7[0] = integrate(7, step6, /*dx*/1, x(2), one);
  fmpq_mpoly_t step8;
  step8[0] = integrate(8, step7, /*dx*/9, zero, x(2));
  fmpq_mpoly_t step9;
  step9[0] = integrate(9, step8, /*dx*/2, zero, x(6));
  fmpq_mpoly_t step10;
  step10[0] = integrate(10, step9, /*dx*/6, zero, one);

  fmpq_mpoly_t result;
  fmpq_mpoly_init(result, ctx);
  fmpz_t fact;
  fmpz_init(fact);
  fmpz_fac_ui(fact, 10);
  fmpq_mpoly_scalar_mul_fmpz(result, step10, fact, ctx);
  /* printf("#le = "); */
  /* fmpq_mpoly_print_pretty(result, (const char**) varNames, ctx); */
  /* printf("\n"); */

  if (fmpq_mpoly_is_fmpq(result, ctx) == 0 ) {
    err("Last step polynomial is not a constant (please report);");
  }
  
  fmpq_t cresult;
  fmpq_init(cresult);
  fmpq_mpoly_get_fmpq(cresult, result, ctx);

  cleanup();
  return cresult[0];
}

fmpq do_integral(int nb_vars, int nb_steps, const Integral integrals[]) {
  init(nb_vars);
  
  fmpq_mpoly_t step;
  fmpq_mpoly_init(step, ctx);
  fmpq_mpoly_one(step, ctx);

  for(int i=1; i <= nb_steps; i++) {
    fmpq_mpoly_t next;
    fmpq_mpoly_init(next, ctx);
    Integral integral = integrals[i-1];
    next[0] = integrate(i, step, integral.var_ref, integral.from_bound, integral.to_bound);
    step[0] = next[0];
  }


/* XXX : if we want the actual linear extension count
   we need to uncomment the following... */
  /* fmpq_mpoly_t result; */
  /* fmpq_mpoly_init(result, ctx); */
  /* fmpz_t fact; */
  /* fmpz_init(fact); */
  /* fmpz_fac_ui(fact, nb_vars); */
  /* fmpq_mpoly_scalar_mul_fmpz(result, step, fact, ctx); */

  /* if (fmpq_mpoly_is_fmpq(result, ctx) == 0 ) { */
  /*   err("Last step polynomial is not a constant (please report);"); */
  /* } */

  /* fmpq_t cresult; */
  /* fmpq_init(cresult); */
  /* fmpq_mpoly_get_fmpq(cresult, result, ctx); */

  /* cleanup(); */
  /* return cresult[0]; */


  /*** and then comment out the folliwing ***/
  if (fmpq_mpoly_is_fmpq(step, ctx) == 0 ) {
    err("Last step polynomial is not a constant (please report);");
  }
  
  fmpq_t cresult;
  fmpq_init(cresult);
  fmpq_mpoly_get_fmpq(cresult, step, ctx);

  cleanup();
  return cresult[0];
}

fmpq do_example_integral_bis() {
  Integral integrals[10] = { { 5, x(0), one }
			     , { 7, zero, x(0) }
			     , { 0, x(3), one } 
			     , { 3, x(1), one }
			     , { 4, zero, x(1) }
			     , { 8, x(1), one }
			     , { 1, x(2), one }
			     , { 9, zero, x(2) }
			     , { 2, zero, x(6) }
			     , { 6, zero, one } };

  fmpq_t result;
  fmpq_init(result);
  result[0] = do_integral(10, 10, integrals);
  return result[0];
}

/*
Example integral
Int_0^1 Int_0^x_6 Int_0^x_2 Int_x_2^1 Int_x_1^1 Int_0^x_1 Int_x_1^1 Int_x_3^1 Int_0^x_0 Int_x_0^1 1 dx_5 dx_7 dx_0 dx_3 dx_4 dx_8 dx_1 dx_9 dx_2 dx_6
*/

fmpq do_example_integral_ter() {
  Integral * integrals;
  size_t size;
  size_t nb_vars;
  integrals = parse_integrals("Int_0^1 Int_0^x_6 Int_0^x_2 Int_x_2^1 Int_x_1^1 Int_0^x_1 Int_x_1^1 Int_x_3^1 Int_0^x_0 Int_x_0^1 1 dx_5 dx_7 dx_0 dx_3 dx_4 dx_8 dx_1 dx_9 dx_2 dx_6", &size, &nb_vars);
  fmpq_t result;
  fmpq_init(result);
  result[0] = do_integral(size, size, integrals);
  free(integrals);
  return result[0];
}


int main(int argc, char** argv) {
  printf("===================\n");
  printf("BIT Integrator v0.1\n");
  printf("===================\n");

  if(argc!=2) {
    printf("Usage: integrator <integral_spec>\n");
    err("Abort");
  }

  fmpq_t count;
  fmpq_init(count);

  // count[0] = do_example_integral();
  // count[0] = do_example_integral_bis();
  // count[0] = do_example_integral_ter();
  
  Integral * integrals;
  size_t size;
  size_t nb_vars;
  integrals = parse_integrals(argv[1], &size, &nb_vars);
  count[0] = do_integral(nb_vars, size, integrals);
  free(integrals);

  printf("===================\n");
  // printf("#le = ");
  fmpq_print(count);
  printf("\n");
  return 0;
}

   
