#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "parse_integral.h"

#define VDEBUG 0

void err(char *msg) {
  fputs("Error:\n", stderr);
  fputs(msg, stderr);
  fputs("\n", stderr);
  exit(EXIT_FAILURE);
}

void print_var(int vref) {
  printf("x%d", vref);
}

void print_bound(Bound bound) {
  switch(bound.kind) {
  case ZERO:
    printf("0");
    break;
  case ONE:
    printf("1");
    break;
  default:
    print_var(bound.val);
  }
}

void print_boundv(void *vbound) {
  print_bound(*((Bound *) vbound));
}

void print_intv(void *vint) {
  int val = *(int *) vint;
  printf("%d", val);
}

typedef struct _darray {
  size_t capacity;
  size_t size;
  void** data;
} darray;

darray * darray_new(size_t capacity, size_t elem_size) {
  darray * arr = (darray *) malloc(sizeof(darray));
  arr->capacity = capacity;
  arr->size = 0;
  arr->data = calloc(capacity, elem_size);
  return arr;
}

void * darray_at(darray* arr, size_t idx) {
  assert(0<=idx);
  assert(idx<arr->size);
  return arr->data[idx];
}

void darray_expand(darray *arr, size_t elem_size) {
  arr->capacity *= 2;
  arr->data = (void **) realloc(arr->data, elem_size * arr->capacity);
}

void darray_add(darray* arr, void * elem) {
  if(arr->size == arr->capacity) {
    darray_expand(arr, sizeof(elem));
  }
  arr->data[arr->size] = elem;
  arr->size += 1;
}

darray * darray_free(darray* arr) {
  free(arr->data);
  arr->data = NULL;
  arr->size = 0;
  arr->capacity = 0,
  free(arr);
  return arr;
}

void darray_print(darray * arr, void (*elem_print)(void *)) {
  printf("[");
  for(size_t i=0; i<arr->size; i++) {
    (*elem_print)(arr->data[i]);
    if(i + 1 < arr->size) {
      printf(", ");
    }
  }
  printf("]");
}

int expect(int pos, char exp, char *input, char *msg) {
  if(pos >= strlen(input)) {
    err("Unexpected end of input.");
  }
  if(input[pos] != exp) {
    err(msg);
  }
  return pos +1;
}

int parse_integer(int* val_ptr, int pos, char *input) {
  char * end;
  int val = strtol(&input[pos], &end, 10);
  if (end == &input[pos]) {
    err("Missing integer");
  }
  char *start = &input[pos];
  while (start!=end) {
    start++;
    pos++;
  }
  *val_ptr = val;
  return pos;
}

int parse_var_bound(Bound* bound_ptr, int pos, char *input) {
  pos = expect(pos, 'x', input, "<should not happen>");
  pos = expect(pos, '_', input, "Missing `_` for variable reference");
  int val;
  pos = parse_integer(&val, pos, input);
  bound_ptr->kind = VAR;
  bound_ptr->val = val;
  return pos;
}

int parse_int_bound(Bound* bound_ptr, int pos, char *input) {
  if (input[pos] == '0') {
    bound_ptr->kind = ZERO;
    bound_ptr->val = 0;
    return pos+1;
  } else if (input[pos] == '1') {
    bound_ptr->kind = ONE;
    bound_ptr->val = 1;
    return pos+1;
  } else {
    err("Missing '0' or '1' or 'x' for integral bound");
    return -1;
  }    
}

Integral * parse_integrals(char* input, size_t *size, size_t *nb_vars) {
  darray *from_bounds = darray_new(64, sizeof(Bound));
  darray *to_bounds = darray_new(64, sizeof(Bound));

  int max_var_ref = -1;

  int pos = 0;
  const int len = strlen(input);
  
  /* 1. Parse from-bounds and to-bounds */
  while(1) {
    // skip spaces
    while(input[pos] == ' ') {
      pos++;
    }
    /* 1. parse the keyword Int */
    if(input[pos] != 'I') {
      break;
    }
    pos++;
    pos = expect(pos, 'n', input, "Missing `Int` keyword.");
    pos = expect(pos, 't', input, "Missing `Int` keyword.");
   
    /* 2. parse bounds */
    pos = expect(pos, '_', input, "Missing `_` before from-bound.");
    
    Bound* from_bound = malloc(sizeof(Bound));
    if(input[pos] == 'x') {
      pos = parse_var_bound(from_bound, pos, input);
      if (from_bound->val > max_var_ref) {
	max_var_ref = from_bound->val;
      }
    } else {
      pos = parse_int_bound(from_bound, pos, input);
    }
    darray_add(from_bounds, from_bound);

    pos = expect(pos, '^', input, "Missing `^` before to-bound.");
    
    Bound* to_bound = malloc(sizeof(Bound));
    if(input[pos] == 'x') {
      pos = parse_var_bound(to_bound, pos, input);
      if (to_bound->val > max_var_ref) {
	max_var_ref = to_bound->val;
      }
    } else {
      pos = parse_int_bound(to_bound, pos, input);
    }
    darray_add(to_bounds, to_bound);

  }

  if(VDEBUG) {
    printf("From bounds = ");
    darray_print(from_bounds, print_boundv);
    printf("\n");
    
    printf("To bounds = ");
    darray_print(to_bounds, print_boundv);
    printf("\n");
  }

  /*  parse the one body */
  while(input[pos] == ' ') {
    pos++;
  }
  pos = expect(pos, '1', input, "Missing '1' Integral body");

 /* Now parse integration variables */

  darray *var_refs = darray_new(64, sizeof(int) *2);

  while(pos < len) {
    // skip spaces
    while(input[pos] == ' ') {
      pos++;
    }
    
    if(input[pos] != 'd') {
      break;
    }
    pos++;
    pos = expect(pos, 'x', input, "Missing `x` for derivative.");
    pos = expect(pos, '_', input, "Missing `_` for  derivative.");
    
    int * val = malloc(sizeof(int));
    pos = parse_integer(val, pos, input);
    darray_add(var_refs, val);
    if (*val > max_var_ref) {
      max_var_ref = *val;
    }

  }

  if (VDEBUG) {
    printf("Variable references = ");
    darray_print(var_refs, print_intv);
    printf("\n");
  }

  /* And finally build the integral */
  Integral * ints_ptr = calloc(var_refs->size, sizeof(Integral));
 
  for(size_t vref=0, bref=from_bounds->size-1; vref < var_refs->size; vref++ , bref--) {
    int var_ref = *((int *) darray_at(var_refs, vref));
    Bound from_bound = *((Bound *) darray_at(from_bounds, bref));
    Bound to_bound = *((Bound *) darray_at(to_bounds, bref));
    ints_ptr[vref] = (Integral) { var_ref, from_bound, to_bound };
  }

  *size = var_refs->size;
  *nb_vars = max_var_ref + 1;
  printf(".. %ld variables\n", *nb_vars);
  printf(".. %ld steps\n", *size);

  // cleanup
  for(size_t i=0;i<var_refs->size;i++) {
    free(var_refs->data[i]);
    free(from_bounds->data[i]);
    free(to_bounds->data[i]);
  }
  var_refs = darray_free(var_refs);
  from_bounds = darray_free(from_bounds);
  to_bounds = darray_free(to_bounds);

  return ints_ptr;
}
