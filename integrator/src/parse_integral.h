
typedef enum _BoundKind 
  { ZERO, ONE, VAR } BoundKind;

typedef struct _Bound {
  BoundKind kind;
  int val;
} Bound;


typedef struct _Integral {
  int var_ref;
  Bound from_bound;
  Bound to_bound;
} Integral;

void err(char *msg);
void print_var(int vref);
void print_bound(Bound bound);
Integral * parse_integrals(char* input, size_t *size, size_t *nb_vars);
