#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <flint/fmpq_mpoly.h>
#include <flint/fmpq_poly.h>
#include <flint/flint.h>
#include <math.h>
#include <time.h>
#include "parse_integral.h"
#include <mpfr.h>
#include <gmp.h>

#define VDEBUG 0

/* Be careful : global variables */
fmpq_mpoly_ctx_t ctx;   // flint context
char ** varNames;       // Storage of variable names (for debugging)
int nbVars;             // number of variables
slong prec = 100;        // precision
flint_rand_t randstate;

#define EPS 100

Bound zero = {ZERO, 0};
Bound one = {ONE, 1};

void init(int nb_vars) {
  // 1. initialization of symbolic variables
  varNames = (char**) malloc(sizeof(char *) * nb_vars);
  nbVars = nb_vars;
  for(int k=0; k<nb_vars; k++) {
    char temp_str[64];
    int nb = snprintf(temp_str, 64, "x%d", k);
    if (nb < 0) {
      err("Cannot build variable (please report)");
    }
      
    varNames[k] = (char *) malloc(sizeof(char) * (nb+1));
    strcpy(varNames[k], temp_str);
  }

  // 2. initialization of polynomials
  fmpq_mpoly_ctx_init(ctx, nb_vars, ORD_LEX);

  /* prec = (slong) fmax(53, ceil(nbVars * log2((float) nbVars)) + 1); */
  
  // 3. Initialize PRNG
  flint_randinit(randstate);
  _flint_rand_init_gmp(randstate);
}

void cleanup() {
  fmpq_mpoly_ctx_clear(ctx);

  // 1. Cleanup variables
  for(int k=0; k<nbVars; k++) {
    free(varNames[k]);
    varNames[k] = (char *) NULL;
  }
  free(varNames);
  varNames = (char **) NULL;

  flint_randclear(randstate);
}

void substitute(fmpq_mpoly_t subst, fmpq_mpoly_t poly, int from_var, int to_var) {
  slong * substs = (slong *) malloc(sizeof(slong) * nbVars);
  for(int var_ref = 0; var_ref < nbVars; var_ref++) {
    if (var_ref == from_var) {
      substs[var_ref] = to_var;
    } else {
      substs[var_ref] = var_ref;
    }
  }
  fmpq_mpoly_compose_fmpq_mpoly_gen(subst, poly, (const slong *) substs, ctx, ctx);
  free(substs);
}

int evaluate(fmpq_mpoly_t eval, fmpq_mpoly_t poly, int var_ref, BoundKind bk) {
  fmpq_t cons;
  fmpq_init(cons);
  switch(bk) {
  case ZERO:
    fmpq_zero(cons);
    break;
  case ONE:
    fmpq_one(cons);
    break;
  default:
      err("Wrong bound kind (please report)");
  }
  
  int res = fmpq_mpoly_evaluate_one_fmpq(eval, poly, var_ref, cons, ctx);
  fmpq_clear(cons);
  return res;
}

fmpq_mpoly_t* compute_primitives(const Integral integrals[], int nb_steps){
  fmpq_mpoly_t* integrals_tab = malloc(sizeof(fmpq_mpoly_t) * (nb_steps + 1));
  fmpq_mpoly_t prim, from, to;
  
  fmpq_mpoly_init(prim, ctx);
  fmpq_mpoly_init(from, ctx);
  fmpq_mpoly_init(to, ctx);
  
  
  for(int i=0; i < nb_steps+1; i++)
    fmpq_mpoly_init(integrals_tab[i], ctx);

  fmpz_t facn;
  fmpz_init(facn);
  fmpz_fac_ui(facn, nb_steps);
  fmpq_mpoly_one(prim, ctx);
  fmpq_mpoly_scalar_mul_fmpz(prim, prim, facn, ctx);
  
  
  for(int i=0; i < nb_steps; i++){
    Integral curr_intop = integrals[i];
    
    fmpq_mpoly_integral(integrals_tab[i], prim, curr_intop.var_ref, ctx);
    
    if(curr_intop.from_bound.kind == VAR)
      substitute(from, integrals_tab[i], curr_intop.var_ref, curr_intop.from_bound.val);
    else
      evaluate(from, integrals_tab[i], curr_intop.var_ref, curr_intop.from_bound.kind);

    if(curr_intop.to_bound.kind == VAR)
      substitute(to, integrals_tab[i], curr_intop.var_ref, curr_intop.to_bound.val);
    else
      evaluate(to, integrals_tab[i], curr_intop.var_ref, curr_intop.to_bound.kind);

    if( i < nb_steps-1)
      fmpq_mpoly_sub(prim, to, from, ctx);
    else
      fmpq_mpoly_sub(integrals_tab[nb_steps], to, from, ctx);
  }

  fmpq_mpoly_clear(prim, ctx);
  fmpq_mpoly_clear(from, ctx);
  fmpq_mpoly_clear(to, ctx);
  return integrals_tab;  
}

void find_root(fmpq_t root, fmpq_poly_t pol, fmpq_t lo, fmpq_t hi){
  fmpq_t mid, fmid, eps, zero, t;
  fmpq_init(mid);
  fmpq_init(fmid);
  fmpq_init(eps);
  fmpq_init(zero);
  fmpq_init(t);

  fmpq_one(lo);
  fmpq_div_2exp(eps, lo, EPS); // eps = 2^-EPS

  fmpq_zero(lo);
  fmpq_zero(zero);
  fmpq_one(hi);
  
  fmpq_sub(t, hi, lo);
  while(fmpq_cmp(t, eps) > 0){
    fmpq_add(mid, hi, lo);
    fmpq_div_2exp(mid, mid, 1);
    fmpq_poly_evaluate_fmpq(fmid, pol, mid);
    if(fmpq_sgn(fmid) < 0)
      fmpq_set(lo, mid);
    else
      fmpq_set(hi, mid);
    fmpq_sub(t, hi, lo);
    fmpq_abs(t, t);
  }

  fmpq_set(root, mid);
  
  fmpq_clear(mid);
  fmpq_clear(fmid);
  fmpq_clear(eps);
  fmpq_clear(zero);
  fmpq_clear(t);  
}

void draw_uniform(fmpq_poly_t u, Integral i, fmpq_poly_struct** point){
  fmpz_t p, q;
  fmpz_init(p);
  fmpz_init(q);
  fmpq_t a, b, x, t;
  fmpq_init(a);
  fmpq_init(b);
  fmpq_init(x);
  fmpq_init(t);

  switch(i.from_bound.kind){
  case ZERO:
    fmpq_zero(a);
    break;
  case ONE:
    fmpq_one(a);
    break;
  default:
    fmpq_poly_get_coeff_fmpq(a, point[i.from_bound.val], 0);
  }

  switch(i.to_bound.kind){
  case ZERO:
    fmpq_zero(b);
    break;
  case ONE:
    fmpq_one(b);
    break;
  default:
    fmpq_poly_get_coeff_fmpq(b, point[i.to_bound.val], 0);
  }
  
  mpfr_t x_mpfr;
  mpq_t x_gmp;
  mpfr_init2(x_mpfr, prec);
  mpq_init(x_gmp);
  mpfr_urandomb(x_mpfr, randstate[0].gmp_state);
  mpfr_get_q(x_gmp, x_mpfr);
  fmpq_set_mpq(x, x_gmp);
  
  fmpq_mul(t, b, x);        // t = b*x
  fmpq_sub_ui(x, x, 1);          // x = x - 1
  fmpq_submul(t, a, x);          // t -= a * x
  fmpq_poly_set_fmpq(u, t);

  fmpz_clear(p);
  fmpz_clear(q);
  fmpq_clear(x);
  fmpq_clear(a);
  fmpq_clear(b);
  fmpq_clear(t);
}
  

fmpq_poly_struct** sampler(Integral integrals[], fmpq_mpoly_t* primitives, int nb_integrals){
  fmpq_poly_t prim, t, u;
  fmpq_t lo, hi, x, a, b, unif;

  fmpq_poly_init(prim);
  fmpq_poly_init(t);
  fmpq_poly_init(u);
  fmpq_init(lo);
  fmpq_init(hi);
  fmpq_init(x);
  fmpq_init(unif);

  mpfr_t x_mpfr;
  mpq_t x_gmp;
  mpfr_init2(x_mpfr, prec);
  mpq_init(x_gmp);

  
  fmpq_poly_struct** point = malloc(sizeof(fmpq_poly_struct*) * nbVars);

  for(int i=0; i<nbVars; i++){
    point[i] = malloc(sizeof(fmpq_poly_struct));
    fmpq_poly_init2(point[i], 2);
    fmpq_poly_set_coeff_si(point[i], 1, 1); // point[i] = x
  }

  for(int i=nb_integrals-1; i>=0; i--){
    fmpq_mpoly_compose_fmpq_poly(prim, primitives[i], point, ctx);

    switch(integrals[i].from_bound.kind){
    case ZERO:
      fmpq_zero(a);
      break;
    case ONE:
      fmpq_one(a);
      break;
    default:
      fmpq_poly_get_coeff_fmpq(a, point[integrals[i].from_bound.val], 0);
    }
    fmpq_poly_evaluate_fmpq(lo, prim, a);

    switch(integrals[i].to_bound.kind){
    case ZERO:
      fmpq_zero(b);
      break;
    case ONE:
      fmpq_one(b);
      break;
    default:
      fmpq_poly_get_coeff_fmpq(b, point[integrals[i].to_bound.val], 0);
    }
    fmpq_poly_evaluate_fmpq(hi, prim, b);

    fmpq_sub(x, hi, lo);
    fmpq_poly_set_fmpq(t, lo);
    fmpq_poly_sub(prim, prim, t);


    mpfr_urandomb(x_mpfr, randstate[0].gmp_state);
    mpfr_get_q(x_gmp, x_mpfr);
    fmpq_set_mpq(unif, x_gmp);

    fmpq_poly_set_fmpq(u, unif);
    fmpq_poly_scalar_mul_fmpq(u,u,x);
    
    fmpq_poly_sub(prim, prim, u);
    find_root(x, prim, a, b);

    fmpq_poly_set_fmpq(point[integrals[i].var_ref], x);
  }
  fmpq_poly_clear(prim);
  fmpq_poly_clear(t);
  fmpq_poly_clear(u);
  fmpq_clear(lo);
  fmpq_clear(hi);
  fmpq_clear(x);

  return point;
}

Bound x(int var_ref) {
  return (Bound) { VAR, var_ref };
}

/* #define N 4 */

int main(int argc, char** argv) {
  printf("===================\n");
  printf("BIT Sampler v0.1\n");
  printf("===================\n");

  if(argc < 2) {
    printf("Usage: sampler <integral_spec> [number of linear extension]\n");
    err("Abort");
  }

  int nb_samples = 1;

  if(argc > 2){
    printf("nb_samples  = %s\n", argv[2]);
    nb_samples = atoi(argv[2]);
  }
  
  Integral * integrals;
  size_t size;
  size_t nb_vars;
  fmpq_poly_struct** point;
  fmpq_t val;

  clock_t start, counting_clock, sampling_clock;
  
  fmpq_init(val);
  integrals = parse_integrals(argv[1], &size, &nb_vars);
  init(nb_vars);

  
  start = clock();
  fmpq_mpoly_t* primitives = compute_primitives(integrals, size);
  counting_clock = clock() - start;
    
  start = clock();  
  for(int k=0; k < nb_samples; k++){
    printf("===================\n");
    point = sampler(integrals, primitives, size);

    for(size_t i = 0; i < nb_vars; i++){
      fmpq_poly_get_coeff_fmpq(val, point[i], 0);
      printf("%.17g", fmpq_get_d(val));
      printf("\n");
    }
  }
  sampling_clock = clock() - start;

  printf("===================\n");
  
  printf("Number of linear extensions: ");
  fmpq_mpoly_print_pretty(primitives[size], (const char**) varNames, ctx);
  printf("\n");
  
  printf("Counting time (s.): %f\n",
         ((float)counting_clock)/CLOCKS_PER_SEC);

  printf("Sampling time (total, in s.): %f\n",
         ((float)sampling_clock)/CLOCKS_PER_SEC);
  
  printf("Sampling time (avg., in s.): %f\n",
         ((float)sampling_clock)/CLOCKS_PER_SEC/nb_samples);
  return 0;    
}

   
