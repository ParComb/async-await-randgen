
from fjseq import enum_fjseq, poset_from_fjseq, fjseq_from_poset, proc_from_fjseq
from ascentseq import enum_ascentseq, poset_to_ascent_sequence, ascent_sequence_to_poset

def fjseq_from_ascentseq(aseq):
    """
    Suppose  fj  is the sequence we build, the process
    is incremental.
    """
    # we will go in decreasing order
    pid = len(aseq)

    # fj is the sequence we want to build, the higher levels is len(fj)-1
    fj = [{pid}] # the last (and unique) process is introduced at level 0
    pid -= 1

   # [(1,2),(2,3),(3,4)]

    # the level of the smallest maximal process
    minlevel = 0

    for i in aseq[1:]:

        # if fj == [{2, 3, 5}, {2, 3, 4}, {2, 4, 6}]:
        #     import pdb ; pdb.set_trace()

        # At each step we introduce a new process at level i

        # print("FJ[{}] = {}".format(i, fj))

        if i <= minlevel:
            # Case 1 : if i <= l*(fj)  then a new process is introduced
            # (from level 0) which joins at level i.
            fork_until(fj, i, pid)
            minlevel = i
    
        elif i == len(fj):  # i == higher_level + 1
            # Case 2 : if i = l(fj) + 1  then we create a new level
            # (level i+1) and the new process forks and joins at that level
            fj = [{pid}] + fj
            minlevel = i

        else: # minlevel < i <= len(fj) - 1
            # first, a new level is introduced at i, and the new process if forked from level 0
            # and joins at that new level.
            fj = fj[:len(fj)-i] + [{pid}] + fj[len(fj)-i:]
    
            # second, all the processes that are alive before and after the new level
            # are forked at the new level, and the join remains untouched.
            max_procs = compute_max_procs(fj)
            update_before_after(fj, i, max_procs)

            minlevel = i
    
        # recompute the minlevel (not needed in all cases ?)
        minlevel = compute_minlevel(fj)

        pid -= 1

    return fj

def fork_until(fj, i, pid):
    for level in range(len(fj)):
        if level <= len(fj) - i - 1:
            fj[level].add(pid)
        else:
            return

def update_before_after(fj, i, max_procs):
    # compute the processe active before level i
    before_procs = set()
    for pids in fj[:-i-1]:
        for bpid in set(before_procs):
            if bpid not in pids:
                before_procs.remove(bpid)
        before_procs.update(pids)
        
    # compute the before processes that at still active after level i
    after_procs = before_procs & fj[-i]

    # put these processes at level i
    pid = None
    for ppid in fj[-i-1]:
        pid = ppid
        break
    fj[-i-1].update(after_procs)

    # and remove the maximal processes among them
    #  from before while making the new process alive
    for j in range(0, len(fj)-i-1):
        fj[j] = fj[j] - (after_procs & max_procs)
        fj[j].add(pid)

def compute_max_procs(fj):
    if len(fj) == 0:
        return set()

    max_procs = fj[0]

    for pids in fj[1:]:
        if max_procs - pids != set():
            # at least one max process has joines, hence they cannot be any new max
            break
        # all the processes are maximum
        max_procs.update(pids)

    return max_procs

def compute_minlevel(fj):
    if len(fj) == 0:
        return 0

    max_procs = compute_max_procs(fj)

    for level in range(len(fj)):
        if max_procs & fj[-level-1] != set():
            minlevel = level
            break

    return minlevel
         

def compare_fj_ascent(size_min=4, size_max=None):
    if size_max is None:
        size_max = size_min
    for size in range(size_min, size_max+1):
        print("Size = {}".format(size))
        print("=========")
        for fj in enum_fjseq(size):
            po = poset_from_fjseq(fj.tolist())
            asc = [i for (i, _) in poset_to_ascent_sequence(po)]
            #if len(fj.tolist()) == 2:
            print(str(fj.tolist()), end="    <===>     ")
            print(str(asc))
            print()

def compare_fj_ascent2(size_min=4, size_max=None):
    if size_max is None:
        size_max = size_min
    for size in range(size_min, size_max+1):
        print("Size = {}".format(size))
        print("=========")
        for aseq in enum_ascentseq(size):
            fj = fjseq_from_ascentseq(aseq) 
            print(str(fj), end="    <===>     ")
            print(str(aseq))
            print()


def show_ascent_decomp(ascent):

    for i in range(1, len(ascent)+1):
        aseq = ascent[:i]

        po = ascent_sequence_to_poset(aseq)

        print("Ascent sequence = {}".format(aseq))
        #print("po={}".format(po))

        fj = fjseq_from_poset(po)
        print("FJ sequence = {}".format(fj))
        #proc = proc_from_fjseq(fj)
        #print("FJ = {}".format(proc.fjstr()))

def show_ascent_decomp2(ascent):
    for i in range(1, len(ascent)+1):
        aseq = ascent[:i]

    

        print("Ascent sequence = {}".format(aseq))
        fj = fjseq_from_ascentseq(aseq)
        print("FJ sequence = {}".format(fj))
    

def check_fjseq_from_ascentseq(size_min=4, size_max=4):
    nb_failures = 0
    for size in range(size_min, size_max+1):
        for aseq in enum_ascentseq(size):
            # method 1
            po =  ascent_sequence_to_poset(aseq)
            fj1 = [set(pids) for pids in fjseq_from_poset(po)]

            # method 2
            fj2 = fjseq_from_ascentseq(aseq)
            if fj1 != fj2:
                nb_failure += 1
                print("ERROR : distinct fj sequences")
                print("  ==> ascent sequence = {}".format(aseq))
                print("  ==> fjseq from po   = {}".format(fj1))
                print("  ==> fjseq from aseq = {}".format(fj2))
    return nb_failures


if __name__ == "__main__":
    import sys
    size_min = 4
    if len(sys.argv) > 1:
        size_min = int(sys.argv[1])
    size_max = size_min
    if len(sys.argv) > 2:
        size_max = int(sys.argv[2])
    #compare_fj_ascent(size_min, size_max)

    print("======================")

    #compare_fj_ascent2(size_min, size_max)

    # show_ascent_decomp([0, 1, 0, 1])
    # show_ascent_decomp2([0, 1, 0, 1])

    # show_ascent_decomp([0, 1, 2, 3, 1, 0, 1, 2])
    # show_ascent_decomp2([0, 1, 2, 3, 1, 0, 1, 2])

    # fj = fjseq_from_ascentseq([0, 1, 0, 1])
    # print("fj = {}".format(fj))

    fj = fjseq_from_ascentseq([0, 1, 2, 3, 1, 0, 1, 2])
    print("fj = {}".format(fj))
    
    print("Checking fjseq <-> ascentseq from size={} to size={}".format(size_min, size_max)) 
    print("======================")

    nb_failures = check_fjseq_from_ascentseq(size_min, size_max)
    if nb_failures == 0:
        print("Success :->")
    else:
        print("Failure :-<  ==> {} errors".format(nb_failures))


    # show_ascent_decomp([0, 1, 0, 1, 0, 1])
    # show_ascent_decomp2([0, 1, 0, 1, 0, 1])

    # #import pdb ; pdb.set_trace()
    # minl = compute_minlevel([{2, 4}, {2, 3}, {3, 5}])
    # print("minlevel={}".format(minl))
