import sys
sys.path.append("../lib")

from poset import *

######### (2+2)-free poset bijection ##########


def remove_next(poset):
    dmap = downset_map(poset)
    dlist = downset_ordering(poset, dmap)
    lvls = levels(poset, dmap, dlist)
    i = minlevel(poset, lvls)

    ilevel = level_elems(poset, i, lvls)
    
    if len(ilevel) > 1:
        # there is at least a max element at level i
        # and there are also other elements
        # thus removing a maximal element preserves the level
        # (simple case)
        for elem in ilevel:
            if poset.ismaximal(elem):
                poset.remove(elem)
                # we take only the first one (non-detersministically)
                return (i, elem)

        raise Exception("No maximal element found (please report)")

    else: # there is only one element at level i
        if i == len(dlist) - 1: # one elment at the top level
            for elem in ilevel:
                poset.remove(elem)
                return (i, elem)

        else: # there are other levels, this is the tricky part
            # we compute the elements that are reachable from
            # the level i+1, removing the single node at level i
            max_elems = dlist[i+1] - dlist[i]
            # and we make the elements maximal in the new poset
            for max_elem in max_elems:
                for big_elem in poset.covering(max_elem):
                    poset.remove_rel(max_elem, big_elem)
            # and finally we remove the element at level i
            for elem in ilevel:
                poset.remove(elem)
                return (i, elem)
            
def add_elem(poset, level, elem):
    if not poset.predecessors:
        if level != 0:
            raise Error("Level must be 0 for one element posets")
        poset.add(elem)
        return

    dmap = downset_map(poset)
    dlist = downset_ordering(poset, dmap)
    lvls = levels(poset, dmap, dlist)
    minlvl = minlevel(poset, lvls)

    levelelems = level_elems(poset, level, lvls)

    if level <= minlvl:
        poset.add(elem, poset.all_predecessors(levelelems))
        return
    elif level == len(dlist):
        max_elems = {elem for elem in poset.predecessors if poset.ismaximal(elem)}
        poset.add(elem, max_elems)
        return
    else: # minlvl < level < len(dlist)
        # we first add the element covering the level predecessors (like in case 1)
        poset.add(elem, poset.all_predecessors(levelelems))
        # we now compute the maximum elements of levels below `level`
        max_elems = set()
        for i in range(0, level):
            for ielem in level_elems(poset, i, lvls):
                if poset.ismaximal(ielem):
                    max_elems.add(ielem)
        # now we compute all minimal elements of higher levels
        min_elems = set()
        for i in range(level, len(dlist)+1):
            min_elems.update(level_elems(poset, i, lvls))
        #import pdb ; pdb.set_trace()
        # we add the same coverings for the new element as the ones from max_elems
        for max_elem in max_elems:
            for min_elem in min_elems:
                #if poset.less_than(min_elem, max_elem): <<< XXX: strange sounds like it's in the def.
                poset.add_rel(max_elem, min_elem)

        # remark : adding the relation requires a transitive reduction
        # oh the poset, so it's a O(n^3) worst-case operation.
        # ... but it doesn't seem to be needed ...
        poset.transitive_reduction()

def poset_to_ascent_sequence(poset, debug=False):
    pos = poset.copy()
    if debug:
        print(pos.predecessors)
    seq = []
    while not pos.isempty():
        i, elem = remove_next(pos)
        if debug:
            print("   ==[remove]==> elem={} at level={}".format(elem, i))
            print(pos.predecessors)
        seq.append((i, elem))
    return seq[::-1]

def rebind_poset(poset):
    nposet = Poset()
    id = 0
    elems = [e for e in poset.predecessors]
    elems.sort()
    rebind = { e : k for (e,k) in zip(elems,range(len(elems), 0, -1))}
    for (elem, elem_preds) in poset.predecessors.items():
        npreds = set()
        for elem_pred in elem_preds:
            npreds.add(rebind[elem_pred])
        nposet.add(rebind[elem], npreds)
    return nposet

def ascent_sequence_to_poset(seq, debug=False):
    pos = Poset()
    counter = 1
    for elem in seq:
        if isinstance(elem, tuple):
            level, label = elem
        else:
            level = elem
            label = "x{}".format(counter)
        counter += 1
        if debug:
            print("-----------------")
            print(pos.predecessors)
            print("  ==add({}@{})==>".format(label,level))
        add_elem(pos, level, label)
        if debug:
            print(pos.predecessors)
    return rebind_poset(pos)

def check_sequence(seq):
    pos = ascent_sequence_to_poset(seq)
    seq2 = [i for (i,_) in poset_to_ascent_sequence(pos)]
    if seq2 != seq:
        print("Mismatch sequences in bijection:")
        print("input sequence = {}".format(seq))
        print("output sequence = {}".format(seq2))
        # does not halt, for now
        return False
    return True
        
def check_all_sequences(size):
    valid = True
    for seq in enum_ascentseq(size):
        if not check_sequence(seq):
            valid = False
    return valid
        
######### Enumeration of ascent sequences ##########


def enum_ascentseq(size, prev=-1, allowed=-1):
    if size==0:
        return [[]]
    
    seqs = []
    for i in range(0, allowed+2):
        nextallow = allowed + (1 if prev<i else 0) 
        nxts = enum_ascentseq(size-1, i, nextallow)
        for nxt in nxts:
            seqs.append([i] + nxt)
    return seqs

def gen_ascentseq(size, prev=-1, allowed=-1):
    if size==0:
        yield ()
        return

    for i in range(0, allowed+2):
        nextallow = allowed + (1 if prev<i else 0) 
        for nxt in gen_ascentseq(size-1, i, nextallow):
            yield ((i,) + nxt)

    return

def unrank_aseq(size, rank):
    i = 0
    for aseq in gen_ascentseq(size):
        if i == rank:
            return aseq
        i += 1
    return None

######### Counting ascent sequences ########

# from https://oeis.org/A022493
# counting ascend sequences
def counting_seq_aux(n, i, t, with_memo=True, memo=None):
    if n <= 1: return 1
    if memo is None:
        memo = dict()
    elif with_memo:
        val = memo.get((n,i,t))
        if val is not None:
            return val

    ret = sum(counting_seq_aux(n - 1, j, t + (j > i), with_memo, memo) for j in range(t + 2))
    memo[(n,i,t)] = ret
    return ret

def counting_ascentseq(n): 
    return counting_seq_aux(n, 0, 0)


######### Examples ##########

Poset_BM = Poset({ "a": {"b", "c", "d"}
                   , "c" : {"f", "g"}
                   , "d" : {"f", "g"}
                   , "e" : {"h"}
                   , "f" : {"h"}
                   , "g" : {"h"}})


Poset_BM2 = Poset({"a" : {"b", "e"}
                   , "b" : {"c", "d", "h"}
                   , "c" : {"g"}
                   , "d" : {"g"}
                   , "e" : {"g"}
                   , "f" : {"g", "h"}})

def main_BM1():
    print("Poset_BM")
    print("--------")
    seq = poset_to_ascent_sequence(Poset_BM, debug=True)
    print(seq)

def main_BM2():
    print("Poset_BM2")
    print("---------")
    seq = poset_to_ascent_sequence(Poset_BM2, debug=True)
    print(seq)

def main_BM2add():
    pos = Poset()
    print("Poset={}".format(pos.predecessors))
    pos.add("a")
    add_elem(pos, 1, "b")
    print(" ==> add (1,'b')")
    print(pos.predecessors)
    add_elem(pos, 2, "c")
    print(" ==> add (2,'c')")
    print(pos.predecessors)
    add_elem(pos, 3, "d")
    print(" ==> add (3,'d')")
    print(pos.predecessors)
    add_elem(pos, 1, "e")
    print(" ==> add (1,'e')")
    print(pos.predecessors)
    add_elem(pos, 0, "f")
    print(" ==> add (0,'f')")
    print(pos.predecessors)
    add_elem(pos, 1, "g")
    print(" ==> add (1,'g')")
    print(pos.predecessors)
    add_elem(pos, 2, "h")
    print(" ==> add (2,'h')")
    print(pos.predecessors)
        
if __name__ == "__main__":
    # main_BM1()
    # main_BM2()
    # main_BM2add()

    # seq4_1 = [0,0,0,0]
    # poset4_1 = ascent_sequence_to_poset(seq4_1)
    # print(poset4_1.predecessors)

    #seq4_2 = [0,0,0,1]
    #poset4_2 = ascent_sequence_to_poset(seq4_2)
    #print(poset4_2.predecessors)

    #seq4_8 = [0,1,0,2]
    #poset4_8 = ascent_sequence_to_poset(seq4_8, debug=True)

    print("Sequences of size 4:")
    seqs4 = enum_ascentseq(4)
    print("\n".join((str(s) for s in seqs4)))

    print("Sequences of size 5:")
    seqs5 = enum_ascentseq(5)
    print("\n".join((str(s) for s in seqs5)))
    
    print("Counting sequences:")
    print(", ".join((str(len(seqs)) for seqs in (enum_ascentseq(n) for n in range(11)))))

    print("Checking counts:")
    print(", ".join((str(counting_ascentseq(n)) for n in range(11)))) 

    print("Checking bijection")
    for size in range(1, 9):
        print("  ==> size {} ".format(size), end='')
        if check_all_sequences(size):
            print("OK")
    
