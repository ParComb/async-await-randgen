'''
Classification of Interval Orders
'''

import sys
sys.path.append("../lib/")
sys.path.append("../bitrec/")
sys.path.append("../../mod-decomp/src/")

from ascentseq import enum_ascentseq, ascent_sequence_to_poset
from ascent_fj_corresp import fjseq_from_ascentseq
from fjseq import poset_from_fjseq
from poclassify import classify_po

import bitrec

def classify_ascentseq(aseq, mode='all'):
    #fj = fjseq_from_ascentseq(aseq)
    #po = poset_from_fjseq(fj)
    po = ascent_sequence_to_poset(aseq)

    bitcat = ''
    if mode == 'BITSTRICT':
        dico = po.to_dict()
        bitpo = bitrec.Poset(dic=dico)
        bitpo.check_invariant()
        isbit, _ = bitrec.test_if_BIT(bitpo)
        if isbit:
            bitcat = 'BIT'
        else:
            bitcat = 'NOBIT'

    cat, _ = classify_po(po)
    if bitcat == 'NOBIT':
        cat = bitcat
    elif bitcat == 'BIT' and cat != 'BIT':
        cat = 'NOBIT'
    return cat

if __name__ == "__main__":

    firstarg = 0
    mode = 'all'
    if len(sys.argv) > 1 and sys.argv[1].startswith("-"):
        if sys.argv[1] == "-sp":
            mode = 'SP'
        elif sys.argv[1] == "-bit":
            mode = 'BIT'
        elif sys.argv[1] == "-bitstrict":
            mode = 'BITSTRICT'
        elif sys.argv[1] == "-split":
            mode = 'SPLIT'
        else:
            raise ValueError(f"No such option: {sys.argv[1]}")
        firstarg = 1
        

    sizemin = 2
    sizemax = 7

    if len(sys.argv) == firstarg + 2:
        sizemin = int(sys.argv[firstarg + 1])
        sizemax = sizemin + 1
    elif len(sys.argv) == firstarg + 3:
        sizemin = int(sys.argv[firstarg + 1])
        sizemax = int(sys.argv[firstarg + 2])

    for size in range(sizemin, sizemax+1):
        #print(f"==== Size {size} ====")
        num = 1
        for aseq in enum_ascentseq(size):
            cat = classify_ascentseq(aseq, mode)
            if (mode == 'BITSTRICT' and cat == 'BIT') \
               or mode == 'all' or cat == mode:
                print(f"[{cat}] {size}/{num} : {aseq}")
 
            num += 1

    
