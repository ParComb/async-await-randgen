
import sys
sys.path.append("../lib")

from promises import *
from poset import *

# A fork/join sequence is simply a succession
# of fork/join operations.

# For example [(/1,2),(2/3),(1/4),(3,4/)] is
# a fork/join sequence explaining the following 3 steps :
#
# - step 1 : two promises with pids 1 and 2 are forked (there cannot be any join at the first step)
# - step 2 : there is a join of pid 2 and then a fork with pid 3
# - step 3 : there is a join of pid 1 and then a fork with pid 4
# - step 4 : The two remaining promises 3,4 are joined. There cannot be a fork at the final step.

# A more compact representation of the same sequence is as follows:
# [(1,2),(1,3),(3,4)]
# This explains the active processes at each step.

##############  Arch processes from Fork/Join sequences ###############

def proc_from_fjseq(fseq, active=None):
    if active is None:
        active = set()

    if not fseq:
        return Join(active)

    # non-empty sequence
    forks = set()
    promises = []
    conts = set()
    fst,*rst = fseq
    for pid in fst:
        if pid in active:
            conts.add(pid)
        else:
            forks.add(pid)
            promises.append(Promise("x{}".format(pid), pid))

    new_active = conts | forks
    cont = proc_from_fjseq(rst, new_active)
    fproc = Fork(forks, promises, cont)
    if active:
        return Join(active - new_active, fproc)
    else:
        return fproc

##############  (2+2)-free Posets from Fork/Join sequences ###############

def poset_from_fjseq(fseq, poset=None, active_forks=None, active_joins=None):
    if poset is None:
        poset = Poset()
    
    if active_forks is None:
        active_forks = set()

    if active_joins is None:
        active_joins = set()

    if not fseq:
        return poset

    # non-empty sequence

    # 1) compute the new forks and the new joins
    new_forks = set()
    conts = set()
    fst,*rst = fseq
    for pid in fst:
        if pid in active_forks:
            conts.add(pid)
        else:
            new_forks.add(pid)

    new_joins = active_forks - conts

    # 2) update the active joins
    new_active_joins = { x for x in active_joins }
    for new_join in new_joins:
        for active_join in active_joins:
            if active_join in new_active_joins and poset.less_than(new_join, active_join):
                new_active_joins.remove(active_join)
        # in all cases add the new join
        new_active_joins.add(new_join)

    if new_active_joins:
        for new_join in new_active_joins:
            poset.add(new_join, new_forks)
            
    else: # just add the new forks
        for new_fork in new_forks:
            poset.add(new_fork)    
            
    new_active_forks = conts | new_forks

    return poset_from_fjseq(rst, poset, new_active_forks, new_active_joins)

def fjseq_from_poset_aux(poset, seq, active, orig_poset):
    if poset.isempty():
        return seq

    max_elems = poset.maximal_elements()

    # 1) computing covers and cover map
    covers = []
    covermap = {}
    for max_elem in max_elems:
        active_cover = frozenset(orig_poset.covering(max_elem) & active)
        if active_cover in covermap:
            covermap[active_cover].add(max_elem)
        else:
            covermap[active_cover] = {max_elem}
            covers.append(active_cover)
            
    covers.sort()
    # Remark : len(covers) > 1    means we have a N-like structure
    # since the covers are subset-related  (otherwise, if disjoint, we would have
    # a (2+2) sub-structure), the only one we can "join" is the smallest one.

    # the processes to join is this covers[0]
    joins = covers[0]
    
    # and the new processes to fork is   covermap[covers[0]]
    new_forks = covermap[covers[0]]
    
    # we must remove the new processes to fork from the poset
    for elem in new_forks:
        poset.remove(elem)
    
    # we must keep the old active ones that were not joined
    new_active = set(new_forks | (active - joins))
    
    seq.append(tuple(new_active))

    return fjseq_from_poset_aux(poset, seq, new_active, orig_poset)
    

def fjseq_from_poset(poset):
    seq = []
    active = set()
    pos = poset.deepcopy()
    return fjseq_from_poset_aux(pos, seq, active, poset)


###################### Enumeration of fork/join sequences ##########################

def fjseq_from_list(lst, fjs=None):
    if fjs is None:
        fjs = FJSeq()

    for elem_pids in lst:
        if fjs.role == 'fork':
            fjs.fork(elem_pids)
        else:
            fjs.join(elem_pids)
    return fjs


class FJSeq:
    def __init__(self, lst=None, pre_active=None):
        self.seq = []
        self.role = 'fork'
        if pre_active:
            self.active = {a for a in pre_active}
        else:
            self.active = set()
        if lst is not None:
            fjseq_from_list(lst, self)

    def copy(self):
        copy = FJSeq()
        copy.seq = self.seq[:]
        copy.role = self.role
        copy.active = { a for a in self.active }
        return copy

    def fork(self, pids):
        if not isinstance(pids, (set, frozenset)):
            pids = {pids}
        if self.role != 'fork':
            raise Exception("Cannot fork, sequence role={}".format(self.role))
        for pid in pids:
            if pid in self.active:
                raise Exception("Cannot fork, process already active: {}".format(pid))
            self.active.add(pid)
        self.seq.append(pids)
        self.role = 'join'

    def join(self, pids):
        if not isinstance(pids, (set, frozenset)):
            pids = {pids}
        if self.role != 'join':
            raise Exception("Cannot join, sequence role={}".format(self.role))
        for pid in pids:
            if pid not in self.active:
                raise Exception("Cannot join, process is not active: {}".format(pid))
            self.active.remove(pid)
        self.seq.append(pids)
        self.role = 'fork'

    def append(self, fjseq):
        for pids in fjseq.seq:
            if self.role == 'fork':
                self.fork(pids)
            else:
                self.join(pids)

    def tolist(self):
        # compact form
        lst = []
        irole = 'F'
        active = set()
        for pids in self.seq:
            if irole == 'F':
                lst.append(tuple(sorted(pids | active)))
                active.update(pids)
                irole = 'J'
            else: # join role
                active.difference_update(pids)
                irole = 'F'
        return lst

    def toshape(self):
        lst = []
        irole = 'F'
        active = set()
        for pids in self.seq:
            if irole == 'F':
                lst.append(len(pids - active))
                active.update(pids)
                irole = 'J'
            else:
                lst.append(-len(pids))
                active.difference_update(pids)
                irole = 'F'
        return lst  

    def __str__(self):
        ret = ""
        irole = 'F'
        for pids in self.seq:
            ret += irole
            ret += "{}".format(tuple(sorted(pids)))
            irole = 'J' if irole == 'F' else 'F'
        return ret

    def __repr__(self):
        return str(self)


class Joinables:
    def __init__(self, joinables=None):
        if joinables is None:
            self.joinables = []
        else:
            self.joinables = sorted(joinables)

    def get_active(self):
        active = set()
        for pids in self.joinables:
            active.update(pids)
        return active

    def join(self, pid):
        njoinables = []
        for joinable in self.joinables:
            if pid in joinable and len(joinable) > 1:
                njoinables.append(joinable - {pid})
        return Joinables(njoinables)

    def joins(self, pids):
        njoinable = self
        for pid in sorted(pids):
            njoinable = njoinable.join(pid)
        return njoinable

    def forks(self, pids):
        # print("----------------------")
        # print("Forks = {}".format(pids))
        # print("Old joinables = {}".format(self.joinables))

        njoinables = [self.joinables[:]]
        level = 0
        for pid in sorted(pids):
            pnjoinables = []
            for njoinable in njoinables[level]:
                pnjoinables.append(njoinable | {pid})
            if level == 0:
                pnjoinables.append({pid})
            level = level + 1
            njoinables.append(pnjoinables)

        rjoinables = []
        for joinables in njoinables:
            rjoinables.extend(joinables)
            
        # print("New joinables = {}".format(rjoinables))

        return Joinables(rjoinables)

    def possible_joins(self):
        return [tuple(sorted(j)) for j in self.joinables]

    def __str__(self):
        return ",".join( (str(tuple(sorted(j))) for j in self.joinables))

    def __repr__(self):
        return str(self)


def all_forks(min_pid, max_pid):
    forks = []
    acc = set()
    for pid in range(min_pid, max_pid+1):
        acc.add(pid)
        forks.append(frozenset(acc))
    return forks

def enum_fjseq(size, nbforks=0, joinables=None):
    if joinables is None:
        joinables = Joinables()  # list of joinable pid sets (in subset ordering)

    if size == nbforks:
        return []

    fjseqs = []

    # 1) compute all possible forks
    forks = all_forks(nbforks+1, size)
    for fork in forks:

        fork_seq = FJSeq(pre_active=joinables.get_active())
        fork_seq.fork(fork)

        # 2) now compute the possible joins for this fork
        fjoinables = joinables.forks(fork)
        
        join_seqs = []

        if size == nbforks + len(fork):
            possible_joins = [fork_seq.active]
        else:
            possible_joins = fjoinables.possible_joins()

        for possible_join in possible_joins:
            join_seq = fork_seq.copy()
            join_seq.join(set(possible_join))

            # 3) now compute all possible continuations
            jjoinables = fjoinables.joins(set(possible_join))
            rest_seqs = enum_fjseq(size, nbforks + len(fork), jjoinables)
            once = False
            for rest_seq in rest_seqs:
                once = True
                fjseq = join_seq.copy()
                fjseq.append(rest_seq)
                # fjseqs.append(fjseq)
                yield fjseq
            
            if not once:
                yield join_seq
                #fjseqs.append(join_seq)

    return None # fjseqs
    

def bijection_main():
    fs1 = [(1,2),(2,3),(2,4)]
    print("forkseq [(1,2),(2,3),(2,4)]")
    print(proc_from_fjseq(fs1))

    ps1 = poset_from_fjseq(fs1)
    print(ps1.predecessors)
    fs1p = fjseq_from_poset(ps1)
    print(fs1p)

    print("-----------------")
    
    fs2 = [(1,2),(2,3),(3,4)]
    print("forkseq [(1,2),(2,3),(3,4)]")
    proc2 = proc_from_fjseq(fs2)
    print(proc2)

    ps2 = poset_from_fjseq(fs2)
    print(ps2.predecessors)
    fs2p = fjseq_from_poset(ps2)
    print(fs2p)

    print("-----------------")

    # The famous "N"-poset
    fs3 = [(1,2), (1,4), (3,4)]
    print("forkseq {}".format(fs3))
    proc3 = proc_from_fjseq(fs3)
    print(proc3)

    ps3 = poset_from_fjseq(fs3)
    print(ps3.predecessors)
    fs3p = fjseq_from_poset(ps3)
    print(fs3p)

def print_fjseq(size):
    print()
    print("Fork/Join sequences of size: {}".format(size))
    print("============================")
    fjseqs = list(enum_fjseq(size))
    #print("\n".join( ("({:>2}) {}".format(k,s.tolist()) for (s,k) in zip(fjseqs, range(1, len(fjseqs)+1)))))
    print("Count = {}".format(len(fjseqs)))


if __name__ == "__main__":
    bijection_main()


    fjs1 = FJSeq([1,1,2,2,3,3,4,4])
    print("fjs1 = {}".format(str(fjs1)))
    print(" ==> compact = {}".format(fjs1.tolist()))

    joinables = Joinables()
    f123 = joinables.forks({1,2,3})
    print("F(1,2,3) = {}".format(str(f123)))
    j1 = f123.join(1)
    print("J(1) = {}".format(str(j1)))
    f45 = j1.forks({4,5})
    print("F(4,5) = {}".format(str(f45)))

    print()
    print("------------------------------")

    print_fjseq(2)
    print_fjseq(3)
    print_fjseq(4)
    print_fjseq(5)
    print_fjseq(6)
    print_fjseq(7)

    print("FJSeq series")
    print("============")
    print(",".join( (str(len(list(seqs))) for seqs in ( enum_fjseq(n) for n in range(1, 10)))))
