import sys
sys.path.append("../lib")

from randgen import *
import poset, posetio
from fjseq import proc_from_fjseq
from ascent_fj_corresp import fjseq_from_ascentseq

if __name__ == '__main__':
    import argparse

    cmdparser = argparse.ArgumentParser(description="Count, enumerate and sample chord processes.")
    cmdparser.add_argument('size', metavar='<size>', action='store', type=int,
                           help='Size of the chord processes (in number of chords)',
                           default=None, nargs='?')
    cmdparser.add_argument('--count', action='store_true',
                           help='Count the number of chord processes of size <size>', default=False)
    cmdparser.add_argument('--sample', action='store_true',
                           help='Sample a random chord process of size <size>', default=False)
    cmdparser.add_argument('--unrank', metavar='<rank>', action='store',
                           type=int, help='Unrank the <rank> chord process of size <size>',
                           default=-1)
    cmdparser.add_argument('--type', choices=['matrix', 'dot'], default='dot',
                           help="The output format: an adjacency matrix or a dot file (graphviz)")
    
    cmdargs = cmdparser.parse_args()

    size = cmdargs.size

    if cmdargs.size is None or ((not cmdargs.count) and
                                (cmdargs.unrank < 0) and
                                (not cmdargs.sample)):
        print("Missing <size> argument and/or command")
        cmdparser.print_usage()

    
    if cmdargs.count:
        n = b(size)
        print("===================== COUNT =====================")
        print(f"The number of size {size} chord processes is: {n}\n")
        
    if cmdargs.sample:
        rank, asc_seq = random_ascent_sequence(cmdargs.size)
        file_ext = ".mat" if cmdargs.type == "matrix" else ".dot"
        filename = f"chord_process_{size}_{rank}" + file_ext
        
        print("===================== SAMPLE =====================")

        proc = proc_from_fjseq(fjseq_from_ascentseq(asc_seq))
        po = poset.Poset(proc.to_dict())
        po.transitive_reduction()

        if cmdargs.type == "matrix":
            with open(filename, 'w') as chord_file:
                posetio.save_poset(po.to_dict(), filename, mode='matrix')
        else:
            with open(filename, 'w') as chord_file:
                chord_file.write(po.to_dot())
        
        print(f"The chord process {rank} as been saved in {filename}")

    if cmdargs.unrank >= 0:
        asc_seq = unrank_ascent_sequence(cmdargs.size, cmdargs.unrank)
        file_ext = ".mat" if cmdargs.type == "matrix" else ".dot"
        rank = cmdargs.unrank
        filename = f"chord_process_{size}_{rank}" + file_ext
        
        print("===================== UNRANK =====================")

        proc = proc_from_fjseq(fjseq_from_ascentseq(asc_seq))
        po = poset.Poset(proc.to_dict())
        po.transitive_reduction()

        if cmdargs.type == "matrix":
            with open(filename, 'w') as chord_file:
                posetio.save_poset(po.to_dict(), filename, mode='matrix')
        else:
            with open(filename, 'w') as chord_file:
                chord_file.write(po.to_dot())
        
        print(f"The chord process {rank} as been saved in {filename}")
