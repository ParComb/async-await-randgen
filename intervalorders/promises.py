
import sys
sys.path.append("../lib")

import poset

def str_dotargs(args):
    return ", ".join(['{}="{}"'.format(k, v) for (k,v) in args.items()])

class DotGraph:
    def __init__(self):
        self.nodes = []
        self.edges = []

    def add_node(self, nodeid, **kwargs):
        self.nodes.append("{} [{}]".format(nodeid
                                           , str_dotargs(kwargs)))

    def add_edge(self, srcid, destid, **kwargs):
        self.edges.append("{} -> {} [{}]".format(srcid, destid
                                                 , str_dotargs(kwargs)))

    def __str__(self):
        dot = "digraph {\n"
        dot += "  rankdir=LR;\n"
        dot += "  /* nodes */\n"
        dot += "\n".join(["  {};".format(node) for node in self.nodes])
        dot += "\n  /* edges */\n"
        dot += "\n".join(["  {};".format(edge) for edge in self.edges])
        dot += " \n}\n"
        return dot
        
def put_dictset(dic, key, val):
    if key in dic:
        dic[key].add(val)
    else:
        dic[key] = {val}

def remove_dictset(dic, key, val):
    vals = dic.get(key)
    if vals is not None:
        if val in vals:
            vals.remove(val)

class Fork:
    def __init__(self, pids, *params):
        self.pids = pids if isinstance(pids, set) else {pids}
        self.promises = params[:-1]
        # hackish
        if len(self.promises) == 1 and isinstance(self.promises[0], list):
            self.promises = self.promises[0]
        self.join = params[-1]

    def tostair(self, stair=None, joinmap=None, time=1):
        if stair is None:
            stair = dict()
        if joinmap is None:
            joinmap = dict()

        for prom in self.promises:
            stair[prom.act] = [time, None]
            put_dictset(joinmap, prom.sync, prom.act)

        return self.join.tostair(stair, joinmap, time+1)

    def todot(self, dot, nmap=None, fcounter=1):
        if nmap is None:
            nmap = dict()
        
        forknode = "fork_{}".format(fcounter)
        dot.add_node(forknode, shape="point", group="main")

        for prom in self.promises:
            promisenode = "prom_{}".format(prom.act)
            dot.add_node(promisenode, label=prom.act)
            if prom.sync not in self.join.pids:
                put_dictset(nmap, prom.sync, promisenode)
            dot.add_edge(forknode, promisenode, arrowhead="none")

        childnmap = {pid:proms for (pid,proms) in nmap.items() if pid not in self.join.pids}
        joinnode = self.join.todot(dot, childnmap, fcounter+1)
        dot.add_edge(forknode, joinnode, style="dashed", arrowhead="none")
        for join_pid in self.join.pids:
            join_proms = []
            join_proms.extend(["prom_{}".format(prom.act) for prom in self.promises if prom.sync == join_pid])
            join_proms.extend(nmap.get(join_pid, set()))
            for join_prom in join_proms:
                #print("Join: {} -[{}]-> {}".format(join_prom, join_pid, joinnode))
                dot.add_edge(join_prom, joinnode, arrowhead="none")
            
        return forknode
        
    def dotfile(self, filename):
        dot = DotGraph()
        self.todot(dot)
        with open(filename, 'w') as f:
            f.write(str(dot))

    def to_dict(self, fid=100):
        control_node = fid
        join_dict = self.join.to_dict(fid+1)
        join_dict[control_node] = self.pids | {fid+1}
        return join_dict

    def __str__(self):
        return "F{}[{}=>{}]".format(str(self.pids)
                                    , "||".join([str(prom) for prom in self.promises])
                                    , str(self.join))

    def fjstr(self):
        return "F{}{}".format(str(self.pids),
                              self.join.fjstr())

    def fjseq(self, active=None):
        if active is None:
            active = set()
        active.update(self.pids)
        return [self.pids | active] + self.join.fjseq(active)

class Promise:
    def __init__(self, act, sync):
        self.act = act
        self.sync = sync

    def __str__(self):
        return "{}.<{}>".format(self.act, self.sync)

class Join:
    def __init__(self, pids, cont=None):
        self.pids = pids if isinstance(pids, set) else {pids}
        self.cont = cont

    def tostair(self, stair, joinmap, time):
        for pid in self.pids:
            for promact in { x for x in joinmap.get(pid, set())}:
                deb, _ = stair[promact]
                stair[promact] = (deb, time)
                remove_dictset(joinmap, pid, promact)

        if self.cont:
            self.cont.tostair(stair, joinmap, time)

        return stair

    def to_dict(self, fid):
        control_node = fid
        if self.cont:
            fork_dict = self.cont.to_dict(fid)
        else:
            fork_dict = dict()
        for pid in self.pids:
            fork_dict[pid] = {control_node}
        return fork_dict
            
            
    def todot(self, dot, nmap, fcounter):
        if self.cont is None:
            dot.add_node("join_end", shape="point", group="main")
            return "join_end"

        # there is a continuation
        cont_node = self.cont.todot(dot, nmap, fcounter)
        # nothing else to do
        return cont_node
        
    def fjstr(self):
        return "J{}{}".format(str(self.pids), "" if self.cont is None else self.cont.fjstr())

    def fjseq(self, active):
        for join_pid in self.pids:
            if join_pid not in active:
                raise ValueError(f"Join pid {join_pid} is not active")
            else:
                active.remove(join_pid)
        if self.cont:
            return self.cont.fjseq(active)
        else:
            return []

    def __str__(self):
        return "J{}{}".format(str(self.pids), "" if self.cont is None else str(self.cont))

def proc_to_svg(proc, prefix="images/proc", dot_path='dot'):
    def find_tempfilename(prefix=prefix+"_", suffix=".dot"):
        for i in range(99999):
            filename = "{}{}{}".format(prefix, i, suffix)
            try:
                f = open(filename, 'r')
                f.close()
            except OSError:
                return filename
            
    dot_filename = find_tempfilename()
    
    with open(dot_filename, 'w') as dot_file:
        dot = DotGraph()
        proc.todot(dot)
        dot_file.write(str(dot))
        
    svg_filename = dot_filename + '.svg'
           
    import subprocess
    ret_code = subprocess.check_call("{dot_path} -Tsvg {dot_filename} -o {svg_filename}".format(**locals()), shell=True)
        
    svg_ret = ""
    with open(svg_filename, 'r') as svg_file:
            svg_ret = svg_file.read()
            
    #import os
    #os.remove(dot_filename)
    #os.remove(svg_filename)
        
    return svg_ret

def show_proc(proc):
    import IPython.display
    return IPython.display.SVG(proc_to_svg(proc))

Fork.show = show_proc

    
Poset_1 = Fork(1, Promise("x", 1), Join(1))
Poset_1p1 = Fork({1,2}, Promise("x1", 1), Promise("x2", 2), Join({1,2}))
Poset_2 = Fork(1, Promise("x1", 1), Join(1, Fork(1, Promise("x2", 1), Join(1))))
Poset_1p1p1 = Fork({1,2,3}
                   , Promise("x1", 1)
                   , Promise("x2", 2)
                   , Promise("x3", 3)
                   , Join({1,2,3}))
Poset_2p1 = Fork({1, 2}
                 , Promise("x1", 1)
                 , Promise("x3", 2)
                 , Join(1, Fork(1, Promise("x2", 1), Join({1, 2}))))
Poset_2on1 = Fork({1,2}
                  , Promise("x1", 1)
                  , Promise("x3", 2)
                  , Join({1, 2}, Fork(1, Promise("x2", 1), Join(1))))
Poset_full4 = Fork({1,2}
                   , Promise("x1", 1)
                   , Promise("x3", 2)
                   , Join({1, 2},
                          Fork({1, 2}
                               , Promise("x2", 1)
                               , Promise("x4", 2)
                               , Join({1, 2}))))
Poset_2on2 = Fork({1,2}
                  , Promise("x1", 1)
                  , Promise("x3", 2)
                  , Join(2, Fork(2, Promise("x4", 2)
                                 , Join(1, Fork(1, Promise("x2", 1)
                                                , Join({1, 2}))))))

Poset_bm = Fork({1,2,3}
                , Promise("a", 1)
                , Promise("b", 2)
                , Promise("c", 3)
                , Join({1,2}, Fork(1, Promise("d", 1)
                                   , Join({1,3}, Fork(1, Promise("e", 1), Join(1))))))


def dotgraph_to_svg(graph, dot_path='dot'):
    def find_tempfilename(prefix="digraph_", suffix=".dot"):
        for i in range(99999):
            filename = "{}{}{}".format(prefix, i, suffix)
            try:
                f = open(filename, 'r')
                f.close()
            except OSError:
                return filename
            
    dot_filename = find_tempfilename()
    
    with open(dot_filename, 'w') as dot_file:
        dot_file.write(graph.to_dot())
        
    svg_filename = dot_filename + '.svg'
           
    import subprocess
    ret_code = subprocess.check_call("{dot_path} -Tsvg {dot_filename} -o {svg_filename}".format(**locals()), shell=True)
        
    svg_ret = ""
    with open(svg_filename, 'r') as svg_file:
            svg_ret = svg_file.read()
            
    import os
    os.remove(dot_filename)
    os.remove(svg_filename)
        
    return svg_ret

def show_dotgraph(graph):
    import IPython.display
    return IPython.display.SVG(dotgraph_to_svg(graph))

DotGraph.show = show_dotgraph
DotGraph.show.__doc__ = "Generates an SVG representation of the graph."

if __name__ == "__main__":
    print("Poset 1 = {}".format(str(Poset_1)))
    #Poset_1.dotfile("poset_1.dot")
    print(Poset_1.tostair())
    print(Poset_1.fjseq())

    print("Poset 1+1 = {}".format(str(Poset_1p1)))
    #Poset_1p1.dotfile("poset_1p1.dot")
    print(Poset_1p1.tostair())

    print("Poset 2 = {}".format(str(Poset_2)))
    #Poset_2.dotfile("poset_2.dot")
    print(Poset_2.tostair())

    print("Poset 1+1+1 = {}".format(str(Poset_1p1p1)))
    #Poset_1p1p1.dotfile("poset_1p1p1.dot")
    print(Poset_1p1p1.tostair())

    print("Poset 2+1 = {}".format(str(Poset_2p1)))
    #Poset_2p1.dotfile("poset_2p1.dot")
    print(Poset_2p1.tostair())

    print("Poset 2/1 = {}".format(str(Poset_2on1)))
    #Poset_2on1.dotfile("poset_2on1.dot")
    print(Poset_2on1.tostair())

    print("Poset full4 = {}".format(str(Poset_full4)))
    #Poset_full4.dotfile("poset_full4.dot")
    print(Poset_full4.tostair())

    print("Poset 2/2 = {}".format(str(Poset_2on2)))
    #Poset_2on2.dotfile("poset_2on2.dot")
    print(Poset_2on2.tostair())

    print("Poset BM = {}".format(str(Poset_bm)))
    #Poset_bm.dotfile("poset_bm.dot")
    print(Poset_bm.tostair())

    
