from functools import lru_cache
from random import randrange
import os

@lru_cache(maxsize=None)
def b(n, prev=-1, allowed=-1):
    # print(n,prev,allowed)
    if n == 0:
        return 1
    else:
        return sum(b(n-1, i , allowed + (1 if prev < i else 0)) for i in range(allowed+2))

def unrank_ascent_sequence(size, num, prev=-1, allowed=-1):
    if size == 0:
        return []
    
    for i in range(allowed+2):
        nextallow = allowed + (1 if prev < i else 0)
        r = b(size-1, i, nextallow)
        if num - r >= 0:
            num -= r
        else:
            return [i] + unrank_ascent_sequence(size-1, num, i, nextallow)


def random_ascent_sequence(size):
    num = randrange(b(size))
    return num, unrank_ascent_sequence(size, num)

if __name__ == "__main__" :
    print(random_ascent_sequence(50))
