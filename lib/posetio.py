"""
A simple Poset importer/exporter library

The import and export functions work from Python
dictionaries representing the poset cover as a DAG
(note that the library does not handle (in)transitivity)

There are three file formats supported :

  1. matrix representation (import/export)

  2. successor lists (import/export)

  3. task dag format  (import only)

"""

def load_poset(fname, mode='auto'):
    with open(fname, 'r') as f:
        nodes = slurp_file(f)
    if len(nodes) == 0:
        return {}

    ### dummy heuristics
    nums = nodes[len(nodes) // 2]

    is_matrix = True
    for num in nums:
        if num != 0 and num != 1:
            is_matrix = False

    if mode == 'matrix' or is_matrix:
        #print(f"Building poset '{fname}' (matrix mode)") 
        return build_matrix(nodes)
    else:
        #print(f"Building poset '{fname}' (successors mode)") 
        return build_successors(nodes)

def slurp_file(f):
    lines = []
    
    while True:
        line = f.readline()
        if line == "":
            return lines
        if not line.lstrip().startswith("#"):
            lines.append([int(e) for e in line.split()])

def add_rel(poset, src, dst):
    successors = poset.get(src, None)
    if successors is None:
        poset[src] = {dst}
    else:
        successors.add(dst)

def build_successors(nodes):
    poset = dict()

    # build relations
    src = 0
    for succs in nodes:
        for dst in succs:
            add_rel(poset, src, dst)
        src += 1

    return poset

def build_matrix(nodes):
    poset = dict()

    # build relations
    for src, line in enumerate(nodes):
        poset[src] = set()
        for dst, val in enumerate(line):
            if val == 1:
                add_rel(poset, src, dst)
                
    return poset

# import task dag
# cf.http://www.loria.fr/~suter/dags.html

class DAGTaskFileReader:
    def __init__(self, file_name):
        self.file = open(file_name, "r")

    def close(self):
        self.file.close()        

    def read(self, n):
        assert (n>0)
        first = self.file.read(1)
        while first=="#":
            self.file.readline() # skip the full comment line
            first = self.file.read(1)
            
        if n==1:
            return first
        else:
            return first + self.file.read(n-1)

    def read_word(self, word):
        rword = self.read(len(word))
        if rword != word:
            raise Exception("Incorrect word read: {0} (expected {1})".format(rword, word))
        return rword

    def read_until(self, end_ch="\n"):
        read_str = ""
        while True:
            ch = self.read(1)
            if ch == "" or ch == end_ch:
                return read_str
            else:
                read_str += ch

        return read_str
            
    def read_node_count(self):
        self.read_word("NODE_COUNT ")
        count_str = self.read_until("\n")
        # print("[DEBUG] Import node count = " + count_str)
        count = int(count_str)
        return count

    def read_node(self):
        self.read_word("NODE ")
        node_id = int(self.read_until(" "))
        children_list = self.read_until(" ")

        # print("[DEBUG] Import node={0} children={1}".format(node_id, children_list))

        if children_list == "-":
            children_ids = set()
        else:
            children_ids = { int(child) for child in children_list.split(",") }

        self.read_until("\n")

        return (node_id, children_ids)
        

def import_task_dag(file_name):
    node_map = dict()
    
    poset = dict()

    reader = DAGTaskFileReader(file_name)
        
    node_count = reader.read_node_count()
    #print("[IMPORT] dag with {} vertices".format(node_count))

    for node in range(node_count):
        node_id, children_ids = reader.read_node()
        if node_id in node_map:
            src = node_map[node_id]
        else:
            src = node_id
            poset[src] = set()
        node_map[node_id] = src
        for child_id in children_ids:
            if child_id in node_map:
                dest = node_map[child_id]
            else:
                dest = child_id
                poset[dest] = set()
            node_map[child_id] = dest

            add_rel(poset, src, dest)

    reader.close()
    return poset


def save_poset(poset, filename, mode='successor'):
    with open(filename, 'w') as f:
        if mode == 'matrix':
            write_poset_matrix(poset, f)
        else:
            write_poset_successors(poset, f)

def write_poset_matrix(poset, f):
    node_ids = sorted(poset.keys())
    for src in node_ids:
        first = True
        for dst in node_ids:
            if first:
                first = False
            else:
                f.write(' ')
            if dst in poset[src]:
                f.write('1')
            else:
                f.write('0')

        f.write("\n")

def write_poset_successors(poset, f):
    node_ids = sorted(poset.keys())
    for src in node_ids:
        first = True
        for dst in poset[src]:
            if first:
                first = False
            else:
                f.write(', ')
            f.write(dst)
        f.write("\n")
        
def usage():
    print("""
Usage: posetio.py <options> <filename> <....>
  options:  -matrix  : matrix mode
            -succ : successors mode
            -taskdag : task-dag import mode
""")

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        usage()
        sys.exit(0)

    mode = 'auto'
    if sys.argv[1] == '-matrix':
        mode = 'matrix'
    elif sys.argv[1] == '-succ':
        mode = 'successors'
    elif sys.argv[1] == '-taskdag':
        mode = 'taskdag'
    
    if mode == 'auto':
        filenames = sys.argv[1:]
    else:
        filenames = sys.argv[2:]

    if len(filenames) == 0:
        print("Error : missing filenames")
        usage()
        sys.exit(-1)

    if mode != 'taskdag':
        for filename in filenames:
            print(f"{filename}: ", end='')
            poset = load_poset(filename, mode)
            print(poset)
    else:
        for filename in filenames:
            print(f"{filename} (taskdag): ", end='')
            poset =  import_task_dag(filename)
            print(poset)


            

