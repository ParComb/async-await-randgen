"""
Conversion of task dags (to matrix representation)
"""

from posetio import *


if __name__ == "__main__":
    import sys
    
    filenames = sys.argv[1:]

    if len(filenames) == 0:
        print("Error : missing filenames")
        sys.exit(-1)

    for filename in filenames:
        print(f"{filename} (taskdag): ", end='')
        poset =  import_task_dag(filename)
        destfname = f"{filename}.txt"
        save_poset(poset, destfname, mode='matrix')
        print("converted", end='')
        poset2 = load_poset(destfname, mode='matrix')
        if poset == poset2:
            print(" [check OK]")
        else:
            print(" [check KO]")
            print(poset)
            print("=================")
            print(poset2)
            sys.exit(-1)




            

