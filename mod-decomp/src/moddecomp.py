import copy

import twostructure as ts
from scc import strongly_connected_components


def pickup(it):
    for thing in it:
        return thing
    raise Exception("Nothing to pickup")


def distinguish(g, w, v1, v2):
    """
    returns true if w distinguishes v1 and v2 in two structure g
    """
    w_v1_col = g.color_of(w, v1)
    if (w,v2) in g.colors[w_v1_col] or (w_v1_col==0 and g.color_of(w,v2)==0):
        v1_w_col = g.color_of(v1, w)
        if (v2, w) in g.colors[v1_w_col] or (v1_w_col==0 and g.color_of(v2,w)==0):
            return False
    return True


def partition_module(g, w, module):
    """
    partitions module in strong maximal modules
    :param g: a two-structure
    :param w: the first node used for partitioning module
    :param module: a set of nodes of g to partition
    :return: a list of sets of nodes
    """
    modules = [ [set() for c in g.colors] for c_ in g.colors ]
    is_empty = [ [True for c in g.colors] for c_ in g.colors ]
    for n in module :
        c_w_n = g.color_of(w, n)  # first color
        c_n_w = g.color_of(n, w)  # second color
        modules[c_w_n][c_n_w].add(n)
        if is_empty[c_w_n][c_n_w] : is_empty[c_w_n][c_n_w] = False
    flattened = [module for l in modules for module in l] # turn 2 dim. matrix into 1 dim
    # removing empty sets :
    for v in is_empty:
        for var in v:
            if var:
                flattened.remove(set())
    return flattened


def maximal_modules(g, v):
    """
    constructs M(g,v) according to the algorithm 3.1 of the article [1]
    :param g: a two-structure
    :param v: a node of g
    :return: a list of sets of nodes (modules)
    """
    init_module = set(g.nodes - {v})
    partition = [init_module]
    outsiders = [ {v} ]
    while True:
        if not partition:
            raise Exception("Empty partition (please report)")

        module = None
        index = 0
        for i in range(len(partition)):
            if len(outsiders[i]) > 0 :
                # pick a module with outsiders
                module = partition[i]  # that's a set of nodes
                index = i
                break
        if module is None:
            # if there is no module left having outsiders
            return partition
        partition.remove(module)
        outsiders_i = outsiders[index]
        outsiders.remove(outsiders_i)
        w = pickup(outsiders_i)  # pick an arbitrary element

        rmodules = partition_module(g, w, module)  # list of sets of nodes
        for rmodule in rmodules:
            partition.append(rmodule)
            a = module - rmodule
            if outsiders_i != None :
                b = a.union(outsiders_i)
            else :
                b = a
            outsiders.append( frozenset(b - {w}))
    return partition # this never happens


def build_quotient(g, partition, v):
    qg = ts.TwoStructure()
    partition.append({v})
    qg.modules = partition
    representatives = [pickup(u) for u in partition] # save?

    l = range(len(partition))
    for i in l:
        for j in l:
            if i != j:
                color = g.color_of(representatives[i], representatives[j])
                if color != 0 : qg.edge(i, j, color) # edge between a representative of qg.modules[i] and one of qg.modules[j]
    qg.nodes = set([ u for u in range(len(representatives)) ])

    return qg


def repr_to_module(qg, r):
    """
    finds the module of a two-structure g represented by the node r in the corresponding quotient graph qg
    :param qg: a quotient graph (two-structure)
    :param r: a node of qg
    :return: the module (set of nodes) of g represented by the node r in qg
    """
    return qg.modules[r]


class DGraph:
    def __init__(self):
        self.nodes = set()
        self.edges = [] # the names of the nodes of a DGraph are consecutive numbers. self.edges[i] contains the set of successors of the node i
        self.modules = []

    def edge(self, src, dst):
        self.nodes.add(src)
        self.nodes.add(dst)
        l = len(self.edges)
        if l <= src:
            self.edges.extend( [ set() for u in range(l, src+1) ] )
        self.edges[src].add(dst)


    def vertices(self):
        return self.nodes


    def successors(self, v):
        """
        returns the set of the successors of the node v
        :param v: a node
        :return:
        """
        if len(self.edges)<=v : return set()
        return self.edges[v]


    def remove_vertex(self, v):
        self.nodes.remove(v)
        l = len(self.edges)
        if v < l:
            self.edges[v] = set()
        self.edges = [ e-{v} for e in self.edges ]


    def node_to_module(self, node):
        """
        returns the module of the initial two-structure corresponding to the node `node` in the distinction graph (self)
        """
        return self.modules[node]


    def to_dot(self):
        dot = "digraph {\n"
        vid = 1
        dot += "  // nodes\n"
        nodemap = dict()
        for node in self.nodes:
            nodemap[node] = vid
            dot += '  N{} [label="{}"];\n'.format(vid, ts.node_to_str(self.node_to_module(node)))
            vid += 1
        dot += "\n  // edges\n"
        for src in range(len(self.edges)) :  # for (src, dsts) in self.edges.items():
            for dst in self.edges[src]:
                dot += '  N{} -> N{};\n'.format(nodemap[src], nodemap[dst])
        dot += "\n}\n"
        return dot


# TO DO : is w always the last module of qg?
def build_distinction_graph(qg, w):
    dg = DGraph()
    dg.modules = qg.modules[:-1] # the last module of qg is w's module
    w = len(qg.modules)-1 # index of w's module in dg.modules list
    dg.nodes = qg.nodes - {w}
    for src in dg.nodes:
        for dst in dg.nodes:
            if src != w and dst != w and distinguish(qg, src, dst, w):
                dg.edge(src,dst)
    return dg


def component_to_str(dg, comp):
    s = "{"
    virg = False
    for nodes in comp:
        if virg:
            s += ", "
        s += ts.node_to_str(dg.node_to_module(nodes))
        virg = True
    s += "}"
    return s


def sinks_to_string(dg, sinks):

    return [ dg.node_to_module(node) for sink in sinks for node in sink ]


class ComponentGraph:
    def __init__(self, graph):
        self.graph = graph
        self.components = strongly_connected_components(graph)
        self.modules = graph.modules


    def node_to_module(self, comp):
        return self.modules[comp]


    def is_sink(self, component):
        for node in component:
            for successor in self.graph.successors(node):
                if successor not in component:
                    return False
        return True


    def remove_component(self, comp):
        for node in comp:
            self.graph.remove_vertex(node)


    def remove_sinks(self):
        # find the sinks
        sinks = []
        ncomps = [] # nodes that are not sinks
        for comp in self.components:
            if self.is_sink(comp):
                sinks.append(comp)
            else:
                ncomps.append(comp)

        for comp in sinks:
            # update the underlying graph
            self.remove_component(comp)

        self.components = ncomps
        return sinks


    def is_empty(self):
        # just to be *really* sure
        return len(self.components) == 0


    def to_dot(self):
        dot = "digraph {\n"
        dot += "  //components\n"
        cid = 1
        nodemap = dict()
        for comp in self.components:
            dot += '  C{} [label="{}"];\n'.format(cid, component_to_str(self,comp))
            for node in comp:
                nodemap[node] = cid
            cid += 1

        dot += "\n  //edges\n"
        edgemap = dict()
        for src in self.graph.nodes:
            for dst in self.graph.successors(src):
                csrc = nodemap[src]
                cdst = nodemap[dst]
                cdests = edgemap.get(csrc)
                if cdests is None:
                    cdests = set()
                    edgemap[csrc] = cdests
                if cdst not in cdests:
                    cdests.add(cdst)
                    dot += '  C{} -> C{};\n'.format(csrc, cdst)

        dot += "\n}\n"
        return dot


def indent(level, spaces=4):
    return " " * (level * spaces)


class TreeNode:
    def __init__(self):
        self.node_type = 'UNKNOWN'
        self.node_colors = []
        self.children = []


    def add_child(self, tnode):
        self.children.append(tnode)


    def tostr(self, level):
        ret = indent(level)
        ret += self.node_type + "\n"
        for child in self.children:
            ret += indent(level) + "|-- "
            ret += child.tostr(level + 1)
        ret += "\n"
        return ret


    def to_decomp(self):
        if self.node_type == 'LEAF':
            return self.node_id
        decomps = [child.to_decomp() for child in self.children]
        return (self.node_type, decomps)

    def node_ids(self):
        if self.node_type == 'LEAF':
            return {self.node_id}
        else:
            node_ids = set()
            for child in self.children:
                node_ids |= child.node_ids()
            return node_ids

    def node_buckets(self):
        if self.node_type == 'LEAF':
            raise ValueError("Cannot ask for node buckets in leaves")

        return [child.node_ids() for child in self.children]

    def __str__(self):
        return self.tostr(0)


def search_prime_graphs(tree):
    if tree.node_type == 'PRIME':
        return [tree.prime_graph]
    
    res = []
    for child in tree.children:
        res += search_prime_graphs(child)
    return res


def modular_decomposition(g):
    """ Modular decomposition of the 2-structure `g`.
    """
    if len(g.nodes) == 1:
        root = TreeNode()
        root.node_type = 'LEAF'
        root.node_id = pickup(g.nodes)
        return root

    # Select an arbitrary node of g
    #print("nodes = {}".format(g.nodes))
    v = pickup(g.nodes)
    #print("Picking node: {}".format(v))
    # Compute the maximal modules of g wrt. v
    mods = maximal_modules(g, v)
    #print("Modules = {}".format(mods))
    # Compute the quotient structure (as a python dictionary)
    qg = build_quotient(g, mods, v)
    #print("Quotient = {}".format(qg.to_dot(True)))
    #ts.twostructure_to_svg(qg)

    # Compute the 'distinction' (?) graph wrt. the image of v
    dg = build_distinction_graph(qg, frozenset(frozenset((v,))))
    #print("DGraph = {}".format(dg.to_dot()))

    # Compute the component graph
    cg = ComponentGraph(dg)
    #print("Component graph = {}".format(cg.to_dot()))

    root = TreeNode()
    u = root
    u.quotient = qg
    while not cg.is_empty():
        w = TreeNode()
        u.add_child(w)
        w.node_type = 'LEAF'
        w.node_id = v ### line missing ###
        sinks = cg.remove_sinks()
        #print("sinks = {}".format(sinks_to_string(cg,sinks)))
        fmodules = list()
        for sink in sinks:  # Get the family of modules in sinks
            for mod in sink:
                fmodules.append(mod)
        #print("fmodules = {}".format(fmodules))
        if len(sinks) == 1 and len(fmodules) > 1:  # Set the type of node u
            u.node_type = 'PRIME' # one complete connected component
            u.prime_graph = qg
        else:
            x = pickup(qg.node_to_module(pickup(fmodules)))
            if g.color_of(x, v) == g.color_of(v, x):
                u.node_type = 'COMPLETE'  # sink is one
                # TODO: SERIES or PARALLEL can be decided here (?)
                u.node_colors = g.color_of(v, x)
            else:
                u.node_type = 'LINEAR' # sinks maybe one or more
                u.node_colors = {g.color_of(x, v),  g.color_of(v, x)}
        #print("u type = {}".format(u.node_type))

        # Recursive calls
        for fmod in fmodules:

            # if len(fmod) <= 1: # XXX : needed ?
            #      w.node = pickup(fmod)
            #      continue

            fg = g.slice(qg.node_to_module(fmod))
            dtree = modular_decomposition(fg)
            #print("dtree = {}".format(dtree.to_decomp()))
            if ((u.node_type == 'COMPLETE' and dtree.node_type == 'COMPLETE')
                and (u.node_colors == dtree.node_colors)) \
                or ((u.node_type == 'LINEAR' and dtree.node_type == 'LINEAR')
                    and (u.node_colors == dtree.node_colors)):
                u.children.extend(dtree.children)
            else:
                u.children.append(dtree)
        u = w

    # print(" = return tree =>\n{}".format(root))
    return root

def reconstruct_prime_graph(dtree, buckets=None):
    """Reconstruct the prime graph from the root node of the
    decomposition tree `dtree`
    """
    pg = TwoStructure()
    if buckets is None:
        buckets = dtree.node_buckets()
    pg.modules = buckets
    qg = dtree.prime_graph
    for (u,v) in qg.colors[1]: #edges
        u_id = -1
        v_id = -1
        for i, b in enumerate(buckets):
            if qg.node_to_module(u) & b:
                u_id = i
            if qg.node_to_module(v) & b:
                v_id = i
        if u_id != v_id and u_id != -1 and v_id != -1:
            pg.edge(u_id, v_id)
    return pg

def reconstruct_prime_graph_dict(dtree, buckets=None):
    """Reconstruct the prime graph from the root node of the
    decomposition tree `dtree`, as an adjascency dictionary
    """
    pg = dict()
    if buckets is None:
        buckets = dtree.node_buckets()
    qg = dtree.prime_graph
    for (u,v) in qg.colors[1]: #edges
        u_id = -1
        v_id = -1
        for i, b in enumerate(buckets):
            if qg.node_to_module(u) & b:
                u_id = i
            if qg.node_to_module(v) & b:
                v_id = i
        if u_id != v_id and u_id != -1 and v_id != -1:
            v_ids = pg.get(u_id, None)
            if v_ids is None:
                v_ids = {v_id}
                pg[u_id] = v_ids
            else:
                v_ids.add(v_id)
    return pg


### unit tests ###

def distinguish_unit_test():
    print("tst1")
    tst1 = ts.TwoStructure()
    tst1.edge(1,2,1)
    tst1.edge(1,3,1)
    print("does 1 distinguish 2 and 3? (no) -> ", distinguish(tst1, 1,2,3), "\n")

    print("tst2")
    tst2 = ts.TwoStructure()
    tst2.edge(2, 1, 1)
    tst2.edge(3, 1, 1)
    print("does 1 distinguish 2 and 3? (no) -> ", distinguish(tst2, 1, 2, 3), "\n")

    print("tst3")
    tst3 = ts.TwoStructure()
    tst3.edge(1, 2, 1)
    tst3.edge(3, 1, 1)
    print("does 1 distinguish 2 and 3? (yes) -> ", distinguish(tst3, 1, 2, 3), "\n")

    print("tst4")
    tst4 = ts.TwoStructure()
    tst4.edge(2, 1, 1)
    tst4.edge(1, 3, 1)
    print("does 1 distinguish 2 and 3? (yes) -> ", distinguish(tst4, 1, 2, 3), "\n")

    print("tst5")
    tst5 = ts.TwoStructure()
    tst5.edge(2, 3, 1)
    print("does 1 distinguish 2 and 3? (no) -> ", distinguish(tst5, 1, 2, 3), "\n")

    print("tst6")
    tst6 = ts.TwoStructure()
    tst6.edge(2, 3, 1)
    tst6.edge(1, 3, 1)
    print("does 1 distinguish 2 and 3? (yes) -> ", distinguish(tst6, 1, 2, 3), "\n")

    print("tst7")
    tst7 = ts.TwoStructure()
    tst7.edge(3, 2, 1)
    tst7.edge(3, 1, 1)
    print("does 1 distinguish 2 and 3? (yes) -> ", distinguish(tst7, 1, 2, 3), "\n")

    print("tst8")
    tst8 = ts.TwoStructure()
    tst8.edge(1, 2, 1)
    tst8.edge(2, 3, 1)
    print("does 1 distinguish 2 and 3? (yes) -> ", distinguish(tst8, 1, 2, 3), "\n")

    print("tst9")
    tst9 = ts.TwoStructure()
    tst9.edge(2, 1, 1)
    tst9.edge(2, 3, 1)
    print("does 1 distinguish 2 and 3? (yes) -> ", distinguish(tst9, 1, 2, 3), "\n")


def unit_test_build_quotient():
    g = ts.TwoStructure()
    g.edge(1,4)
    g.edge(1,3)
    g.edge(1,2)
    g.edge(9,4)
    g.edge(9,8)
    g.edge(9,2)
    g.edge(9,1)
    g.edge(8,4)
    g.edge(8,3)
    g.edge(8,2)
    g.edge(8,1)
    g.edge(7,6)
    g.edge(7,8)
    g.edge(7,9)
    g.edge(6,5)
    g.edge(6,8)
    g.edge(6,9)
    g.edge(5,8)
    g.edge(5,9)
    partition = []
    partition.append({2,3,4})
    partition.append({8,9})
    partition.append({5,6,7})
    q = build_quotient(g, partition, 1)
    print(list(q.all_edges()))
    return q


def unit_test_build_distinction_graph():
    qg = unit_test_build_quotient()
    dg = build_distinction_graph(qg, 3)
    print()
    print(qg.modules)
    print(dg.modules)
    print(dg.edges)
    return dg


def unit_test_strongly_connected_components():
    dg = unit_test_build_distinction_graph()
    scc = strongly_connected_components(dg)
    print()
    print(scc)


def unit_test_partition_module():
    g = ts.graph_ex1()
    p = partition_module(g, 1, g.nodes - {1})
    print(p)


def unit_test_maximal_modules():
    g = ts.graph_ex1()
    print(g.all_edges,"\n")
    mod = maximal_modules(g, 1)
    print(mod)


def unit_test_modular_decomposition():
    g = ts.graph_ex1()
    tree = modular_decomposition(g)
    print("tree = {}".format(tree.to_decomp()))

