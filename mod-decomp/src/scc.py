
### strongly connected components
### (Tarjan algorithm)

class SCCNode:
    def __init__(self, vertex, index):
        # original vertex ... must be hashable and unique (among vertices)
        self.vertex = vertex
        self.index = index
        self.lowlink = index
        self.on_stack = False


class SCC:
    def __init__(self, graph):
        self.graph = graph
        self.index = 0
        self.components = []
        self.comp_index = 0
        self.dfs_stk = []
        self.visited = dict()
    
    def compute(self):
        for v in self.graph.vertices():
            if v in self.visited:
                continue
            # v has not been visited
            vnode = SCCNode(v, self.index)
            self.index += 1
            self.visited[v] = vnode
            self.visit(vnode)

    def visit(self, vnode):
        self.dfs_stk.append(vnode)
        vnode.on_stack = True
        
        for w in self.graph.successors(vnode.vertex):
            if w not in self.visited:
                # recurse
                wnode = SCCNode(w, self.index)
                self.index += 1
                self.visited[w] = wnode
                self.visit(wnode)
                vnode.lowlink = min(vnode.lowlink, wnode.lowlink)
            else:
                wnode = self.visited[w]
                if wnode.on_stack:
                    vnode.lowlink = min(vnode.lowlink, wnode.index)

        if vnode.lowlink == vnode.index:
            component = set()
            while True:
                wnode = self.dfs_stk.pop()
                wnode.on_stack = False
                component.add(wnode.vertex)
                if wnode is vnode:
                    break
            self.components.append(component)
            
def strongly_connected_components(graph):
    """Compute the strongly connected components of `graph`.
    The graph must have a `vertices()` iterator
    as well as a `successors(v)` iterator for the successors
    of vertex `v` in the graph.
    Returns a list of sets of vertices.
    """
    scc = SCC(graph)
    scc.compute()
    return scc.components

        
class MiniGraph:
    def __init__(self, dico):
        self.dico = dico

    def vertices(self):
        return self.dico.keys()

    def successors(self, v):
        return self.dico[v]

def digraph_ex1():
    g = MiniGraph({1 : {2},
                   2 : {3},
                   3 : {1},
                   4 : {2, 3, 5},
                   5 : {4, 6},
                   6 : {3, 7},
                   7 : {7, 6},
                   8 : {5, 7, 8}})
    return g

if __name__ == "__main__":
    print(strongly_connected_components(digraph_ex1()))
