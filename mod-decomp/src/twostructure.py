class TwoStructure:
    def __init__(self, graph=None):
        # the graph of a two structure
        self.nodes = set()
        self.colors = [set()] # list of sets, self.colors[i] contains the set of edges of color i
        self.modules = [] # only for quotient graph, contains the nodes of the graph if they are modules

    def node_to_module(self, node):
        """
        returns the module of a two-structure corresponding to the node `node` of its quotient graph
        """
        return self.modules[node]

    """
    # TODO : v2 : maybe list comprehension can be improved
    def slice(self, nodes):
        sts = TwoStructure()
        sts.nodes = set(nodes)
        edges = set( [ (u,v) for u in nodes for v in nodes] )
        sts.colors = [ x.intersection(edges) for x in self.colors ]
        return sts
    """
    # v2.2
    def slice(self, nodes):
        sts = TwoStructure()
        sts.nodes = set(nodes)
        for c in self.colors[1:]:
            c_edges = set()
            for edge in c:
                if edge[0] in sts.nodes and edge[1] in sts.nodes:
                    c_edges.add(edge)
            sts.colors.append(c_edges)
        return sts

    def edge(self, src, dst, col=1):
        if col == 0 or col == None:
            raise ValueError("Color 0 is reserved (no edge marker)")
        for color in range(1, len(self.colors)):
            if color != col and (src, dst) in self.colors[color]:
                # TODO : is it necessary not to allow recoloring?
                raise ValueError("Edge already existing with distinct color: {} -> {} (color {}))".format(src, dst, col))
        l = len(self.colors)
        if col >= l:
            self.colors.extend( [ set() for i in range(l, col+1)] )
        self.colors[col].add((src, dst))
        self.nodes.add(src)
        self.nodes.add(dst)

    def uedge(self, src, dst, col=1):
        self.edge(src, dst, col)
        self.edge(dst, src, col)


    def edges_zero(self):
        """
        return 0-color edges
        """
        all_edges = set( [ (u,v) for u in self.nodes for v in self.nodes ] )
        same_src_dst_edges = set( [ (u,u) for u in self.nodes ] )
        all_edges = all_edges - same_src_dst_edges
        for edge_set in self.colors:
            all_edges = all_edges - edge_set
        return all_edges


    def all_edges(self, col=None):
        """
         :return: triplet of source, destination and color for all edges
        """
        for edge in self.edges_zero():
            yield (edge[0], edge[1], 0)
        for color in range(len(self.colors)):
            for edge in self.colors[color]:
                yield (edge[0], edge[1], color)


    def color_of(self, v1, v2):
        for color in range(1, len(self.colors)):
            if (v1, v2) in self.colors[color]:
                return color
        return 0


    def to_dot(self, show_zero=False, simple=False):
        dot = "digraph {\n\n"

        dot += "  // nodes\n\n"

        nodenames = dict()
        node_index = 1
        for node in self.nodes:
            nodename = "N{}".format(node_index)
            nodenames[node] = nodename
            dot += '  {} [label="{}"];\n'.format(nodename,
                                                 node_index if simple
                                                 else str(node_to_str(self.node_to_module(node))))
            node_index += 1
            
        dot += "  // edges\n\n"
         
        for (src, dst, col) in self.all_edges():
            if col != 0:
                dot += '  {} -> {} [label="{}"];\n'.format(nodenames[src], nodenames[dst],
                                                           "" if simple else col)
            elif show_zero:
                dot += '  {} -> {} [style=dotted];\n'.format(nodenames[src], nodenames[dst])

        dot += "}\n"

        return dot


# Ugly hack
def node_to_str(node):
    if isinstance(node, frozenset):
        return str(set(node))
    else:
        return str(node)


def twostructure_to_svg(poset, show_zero=False, simple=False, prefix="twostruct", dot_path='dot'):
    def find_tempfilename(prefix=prefix+"_", suffix=".dot"):
        for i in range(99999):
            filename = "{}{}{}".format(prefix, i, suffix)
            try:
                f = open(filename, 'r')
                f.close()
            except OSError:
                return filename

    dot_filename = find_tempfilename()

    with open(dot_filename, 'w') as dot_file:
        dot_file.write(poset.to_dot(show_zero, simple))

    svg_filename = dot_filename + '.svg'

    import subprocess
    ret_code = subprocess.check_call("{dot_path} -Tsvg {dot_filename} -o {svg_filename}".format(**locals()), shell=True)

    svg_ret = ""
    with open(svg_filename, 'r') as svg_file:
        svg_ret = svg_file.read()

    import os
    os.remove(dot_filename)
    #os.remove(svg_filename)

    return svg_ret

def show_twostruct(struct, show_zero=False, simple=False):
    import IPython.display
    return IPython.display.SVG(twostructure_to_svg(struct, show_zero, simple))

TwoStructure.show = show_twostruct

### build two_structure ###

def twostructure_from_dict(dico, color=1):
    """Build a two structure from the provided `dico`
    which must represent an adjascency dictionary.
    Each key is a node `src`, and if a node `dst` is
    an element of `dico[src]`, then there will be an
    edge from `src` to `dst` in the resulting two structure.
    The `color` used is 1 by default, and all edges will have
 the same global color.
    """
    graph = TwoStructure()
    graph.nodes = { node for node in dico }
    for (src, dests) in dico.items():
        for dst in dests:
            graph.edge(src, dst, color)
    return graph

### digraph examples ###
        
def graph_ex1():
    """An example of an undirected graph as a 2-structure.
    from: A survey of the algorithmic aspects of modular decomposition.
          Michel Habib, Christophe Paul.
          Computer Science Review 4 (2010).
    """
    g = TwoStructure()
    g.uedge(1, 2)
    g.uedge(1, 4)
    g.uedge(1, 3)
    g.uedge(2, 4)
    g.uedge(2, 5)
    g.uedge(2, 6)
    g.uedge(2, 7)
    g.uedge(4, 5)
    g.uedge(4, 6)
    g.uedge(4, 7)
    g.uedge(3, 4)
    g.uedge(3, 5)
    g.uedge(3, 6)
    g.uedge(3, 7)
    g.uedge(5, 6)
    g.uedge(5, 7)
    g.uedge(6, 8)
    g.uedge(6, 9)
    g.uedge(6, 11)
    g.uedge(6, 10)
    g.uedge(7, 8)
    g.uedge(7, 9)
    g.uedge(7, 11)
    g.uedge(7, 10)
    g.uedge(8, 9)
    g.uedge(8, 11)
    g.uedge(8, 10)
    g.uedge(9, 11)
    g.uedge(9, 10)
    return g

def digraph_ex1():
    """An exeample of a directed graph as a 2 structure.
    From: Chrisophe Paul's HDR, http://www.lirmm.fr/%7Epaul/HdR/hdr.pdf
    """
    g = TwoStructure()
    g.edge(1, 2)
    g.edge(1, 3)
    g.edge(2, 3)
    g.edge(4, 1)
    g.edge(4, 2)
    g.edge(4, 3)
    g.edge(4, 6)
    g.edge(5, 1)
    g.edge(5, 2)
    g.edge(5, 3)
    g.edge(5, 6)
    g.edge(6, 4)
    g.edge(6, 5)
    g.edge(6, 7)
    g.edge(6, 8)
    g.edge(7, 8)
    g.edge(8, 7)
    return g

def linear_ex():
    g = TwoStructure()
    g.edge(1, 2)
    g.edge(1, 3)
    g.edge(2, 3)
    return g

def diamant_ex1():
    g = TwoStructure()
    g.edge(1, 2)
    g.edge(1, 3)
    g.edge(1, 4)
    g.edge(2, 4)
    g.edge(3, 4)
    return g


def sp_ex1():
    g = TwoStructure()
    for dst in range(2, 8):
        g.edge(1, dst)
    g.edge(2, 7)
    for dst in range(4, 8):
        g.edge(3, dst)
    for dst in range(6, 8):
        g.edge(4, dst)
        g.edge(5, dst)
    g.edge(6, 7)
    return g


def ex1():
    g = TwoStructure()
    for dst in range(1, 4):
        g.edge(dst, 4)
        g.edge(dst, 5)
        g.edge(4, dst)
        g.edge(5, dst)
    for dst in range(6, 10):
        g.edge(dst, 4)
        g.edge(dst, 5)
        g.edge(4, dst)
        g.edge(5, dst)
    return g


def ex2():
    g = TwoStructure()
    for dst in range(1, 4):
        g.edge(dst, 4)
        g.edge(dst, 5)
        g.edge(4, dst)
        g.edge(5, dst)
    for dst in range(4, 6):
        g.edge(6, dst)
        g.edge(8, dst)
        g.edge(dst, 7)
        g.edge(dst, 9)
    return g


def graph5():
    g = TwoStructure()
    for src in range(0, 5):
        for dst in range(src+1, 5):
            g.edge(src, dst)
    return g


def graph5c():
    g = TwoStructure()
    for src in range(0, 5):
        for dst in range(src+1, 5):
            g.edge(src, dst)
            g.edge(dst, src)
    return g


def graph5_1():
    g = TwoStructure()
    for dst in range(0, 5):
        g.edge(0, dst)
    g.edge(1, 0)
    g.edge(1, 2)
    for dst in range(1, 5):
        g.edge(2, dst)
    g.edge(3, 2)
    g.edge(3, 0)
    g.edge(3, 4)

    g.edge(4, 2)
    g.edge(4, 0)
    g.edge(4, 3)

    return g


def graph5_1c():
    g = TwoStructure()
    for dst in range(0, 5):
        g.edge(0, dst)
        g.edge(dst,0)
    g.edge(1, 0)
    g.edge(1, 2)
    g.edge(0, 1)
    g.edge(2, 1)
    for dst in range(1, 5):
        g.edge(2, dst)
        g.edge(dst, 2)
    g.edge(3, 2)
    g.edge(3, 0)
    g.edge(3, 4)
    g.edge(2, 3)
    g.edge(0, 3)
    g.edge(4, 3)

    g.edge(4, 2)
    g.edge(4, 0)
    g.edge(4, 3)
    g.edge(2, 4)
    g.edge(0, 4)
    g.edge(3, 4)

    return g


def graph5_2c():
    g = TwoStructure()
    for dst in range(1, 5):
        g.edge(0, dst)
    g.edge(1, 0)
    g.edge(1, 2)

    g.edge(2, 0)
    g.edge(2, 1)
    g.edge(2, 3)

    g.edge(3, 0)
    g.edge(3, 2)
    g.edge(3, 4)

    g.edge(4, 0)
    g.edge(4, 3)

    return g


def graph5_2():
    g = TwoStructure()
    for dst in range(1, 5):
        g.edge(0, dst)
    g.edge(1, 2)
    g.edge(2, 3)
    g.edge(4, 0)
    g.edge(3, 4)

    return g


def graph5_2_1():
    g = TwoStructure()

    g.edge(0, 1)
    g.edge(0, 3)
    g.edge(0, 4)

    g.edge(1, 2)
    g.edge(1, 4)

    g.edge(2, 3)

    g.edge(3, 4)
    g.edge(4,0)
    g.edge(4,1)
    g.edge(4,3)

    return g


def graphlin1():
    g = TwoStructure()

    g.edge(0, 1)
    g.edge(1, 2)
    g.edge(2, 3)

    g.edge(1, 0)
    g.edge(2, 1)
    g.edge(3, 2)

    return g


def graphlin2():
    g = TwoStructure()

    g.edge(0, 1)
    g.edge(1, 2)
    g.edge(2, 3)

    return g


def graphlin3():
    g = TwoStructure()

    for src in range(0,4):
        for dst in range(src+1, 4):
            g.edge(src, dst)

    return g


def graph5_2_1c():
    g = TwoStructure()

    g.edge(0, 1)
    g.edge(0, 3)
    g.edge(0, 4)

    g.edge(1, 0)
    g.edge(1, 2)
    g.edge(1, 4)

    g.edge(2, 1)
    g.edge(2, 3)

    g.edge(3, 0)
    g.edge(3, 2)
    g.edge(3, 4)

    g.edge(4, 0)
    g.edge(4, 1)
    g.edge(4, 3)

    return g


def graph5_3c():
    g = TwoStructure()

    g.edge(0, 1)
    g.edge(0, 3)
    g.edge(0, 4)

    g.edge(1, 0)
    g.edge(1, 2)

    g.edge(2, 1)
    g.edge(2, 3)

    g.edge(3, 2)
    g.edge(3, 4)
    g.edge(3, 0)

    g.edge(4, 0)
    g.edge(4, 3)

    return g


def g19_3__1():
    g = TwoStructure()
    for i in range(6,10):
        g.edge(0,i)
        g.edge(i,0)
    for i in range(6,10):
        for j in range(i+1,10):
            g.edge(i,j)
            g.edge(j,i)

    g.edge(9,1)
    g.edge(1,9)
    g.edge(1,2)
    g.edge(2,3)
    g.edge(3,4)
    g.edge(3,5)
    g.edge(4,5)

    return g


def g20_3__linear1():
    g = TwoStructure()
    for i in range(10):
        for j in range(i+1, 10):
            g.edge(i,j)
    return g


def g20_3__linear2():
    g = TwoStructure()

    for i in range(6, 10):
        g.edge(5,i)
    for i in range(7, 10):
        g.edge(6, i)
    for i in range(8, 10):
        g.edge(7, i)
    for i in range(9, 10):
        g.edge(8, i)

    for i in range(1, 10):
        g.edge(0,i)
    for i in range(2, 10):
        g.edge(1, i)
    for i in range(3, 10):
        g.edge(2, i)
    for i in range(4, 10):
        g.edge(3, i)
    for i in range(5, 10):
        g.edge(4, i)

    return g


def g20_3():
    g = TwoStructure()
    g.edge(0,1)
    g.edge(1,0)
    g.edge(0,6)
    g.edge(0,1)
    for i in range(1,6):
        for j in range(i+1,6):
            g.edge(i,j)
            g.edge(j,i)
    for i in range(6, 10):
        for j in range(i+1,10):
            g.edge(i,j)
    return g


def g20_complete():
    g = TwoStructure()
    for i in range(5):
        for j in range(i+1,5):
            g.edge(i,j)
            g.edge(j,i)

    """
    g.edge(0,1)
    g.edge(1,0)
    g.edge(1,2)
    g.edge(2,1)
    g.edge(0, 2)
    g.edge(2, 0)
    """

    return g


def g20_prime():
    g = TwoStructure()

    for i in range(5):
        g.edge(i, i+1)

    return g

def g22_3_complete_linear():
    g = TwoStructure()
    for i in range(3):
        for j in range(i+1,3):
            g.edge(i,j)
            g.edge(j,i)
    for i in range(2,5):
        for j in range(i+1,5):
            g.edge(i,j)
    return g

def g22_3_lcl():
    g = TwoStructure()
    for i in range(6):
        for j in range(i+1,6):
            g.edge(i,j)
    for i in range(3,6):
        for j in range(i+1,6):
            g.edge(j,i)
    for i in range(3,9):
        for j in range(6, 9):
            g.edge(i, j)
    return g


def g22_3_2():
    g = TwoStructure()
    for i in range(1,6):
        g.edge(0,i)
    for i in range(2,6):
        g.edge(1, i)
    for i in range(3,6):
        g.edge(2, i)
    for i in range(4,6):
        g.edge(3, i)
    g.edge(4,5)
    g.edge(3,2)
    return g

def g22_3_3():
    g = TwoStructure()
    for i in range(1,6):
        g.edge(0,i)
    for i in range(2,6):
        g.edge(1, i)
    for i in range(4,6):
        g.edge(2, i)
    for i in range(4,6):
        g.edge(3, i)
    g.edge(4,5)
    g.edge(3,2)
    return g

def g22_3_4():
    g = TwoStructure()
    for i in range(1,6):
        g.edge(0,i)
    for i in range(2,6):
        g.edge(1, i)
    for i in range(4,6):
        g.edge(2, i)
    for i in range(4,6):
        g.edge(3, i)
    g.edge(4,5)
    g.edge(3,6)
    g.edge(6, 2)
    return g

def g22_3_5():
    g = TwoStructure()
    for i in range(1,6):
        g.edge(0,i)
    for i in range(2,6):
        g.edge(1, i)
    for i in range(4,6):
        g.edge(2, i)
    for i in range(4,6):
        g.edge(3, i)
    g.edge(4,5)
    g.edge(3,6)
    g.edge(6, 2)
    for i in range(3):
        g.edge(i, 6)
    return g



