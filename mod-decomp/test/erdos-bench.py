"""
tests for moddecomp.py on erdos-renyi graphs
"""


######################### results #############################

# n = 100   p = 0.02    ->   0,11 s
# n = 1000  p = 0.0002  ->   1,46 s  (~100 edges)
# n = 10000 p = 0.00002 ->   1615 s (~1000 edges)

###############################################################



import sys
sys.path.append("../src")

import time
from twostructure import *
from moddecomp import *
from scc import *
from networkx.generators.random_graphs import erdos_renyi_graph


def test(n=1000, p=0.0002):
    """
    mesures execution time
    :param n: number of nodes
    :param p: probability of a pair of nodes to be included in the edges
    """
    g = erdos_renyi_graph(n, p)
    G = TwoStructure()
    for e in g.edges:
        G.edge(e[0], e[1], 1)
        G.edge(e[1], e[0], 1)
    print(len(g.edges), " edges")
    print("testing...")
    t = time.time()

    tree = modular_decomposition(G)

    t2 = time.time() - t
    print(t2)
    print()


def test_stats(n=1000, p=0.0002):
    """
    saves performance files
    :param n: number of nodes
    :param p: probability of a pair of nodes to be included in the edges
    """
    g = erdos_renyi_graph(n, p)
    G = TwoStructure()
    for e in g.edges:
        G.edge(e[0], e[1], 1)
        G.edge(e[1], e[0], 1)
    print(len(g.edges), " edges")
    print("testing...")

    import cProfile
    cProfile.runctx('g(x)', {'x': G, 'g': modular_decomposition}, {}, filename="output.dat")
    import pstats
    with open("output_data_calls.txt", 'w') as f:
        p = pstats.Stats("output.dat", stream=f)
        p.sort_stats("ncalls").print_stats()
    with open("output_data_time.txt", 'w') as f:
        q = pstats.Stats("output.dat", stream=f)
        q.sort_stats("time").print_stats()


if __name__ == "__main__" :
    #test()
    test_stats(1000)
